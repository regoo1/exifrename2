//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "tabwidget.h"
#include "mainwindow.h"
#include "ui_tabwidget.h"
#include "info.h"
#include "filters.h"
// private slots:
void TabWidget::defaultExtensions()
{
    QMessageBox *msgBox = new QMessageBox(this);
    QAbstractButton *noButton = msgBox->addButton(tr("No"), QMessageBox::NoRole);
    QAbstractButton *yesButton = msgBox->addButton(tr("Yes"), QMessageBox::YesRole);
    msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("Do you want to delete all saved file extensions and restore the file extension filters to the default values?"));
//    msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
    msgBox->exec();

    if(msgBox->clickedButton() == noButton) {
        delete yesButton;
        delete msgBox;
        return;
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    // settings.setValue("selectedfilters", Filters::allFilters());
    settings.endGroup();
    ui->cmbExtensions->clear();
    // ui->cmbExtensions->addItems(Filters::allFilters());
    delete yesButton;
    delete msgBox;
}
void TabWidget::newExtensions()
{
    QInputDialog *dialog = new QInputDialog(this);
    dialog->setInputMode(QInputDialog::TextInput);
    dialog->setFont(*fmono);
    dialog->setCancelButtonText(tr("Cancel"));
    dialog->setOkButtonText(tr("OK"));
    dialog->setWindowTitle(DISPLAY_NAME " " VERSION);
    dialog->setLabelText(tr("Name [*.extension1 *.extension2] (*.extension1 *.extension2)"));
    dialog->setTextValue(tr("My image files [*.jpg *.rw2] (*.jpg *.rw2)"));

    if(dialog->exec() == QDialog::Accepted) {
        QString text = dialog->textValue();
        QStringList selectedfilters;
        ui->cmbExtensions->addItem(text);

        for(int i = 0; i < ui->cmbExtensions->count(); i++) {
            selectedfilters << ui->cmbExtensions->itemText(i);
        }

        selectedfilters.removeDuplicates();
        selectedfilters.sort(Qt::CaseInsensitive);
        ui->cmbExtensions->clear();
        ui->cmbExtensions->addItems(selectedfilters);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Extensions");
        settings.setValue("selectedfilters", selectedfilters);
        settings.endGroup();
    }
}

void TabWidget::removeExtensions()
{
    QStringList selectedfilters;

    for(int i = 0; i < ui->cmbExtensions->count(); i++) {
        selectedfilters << ui->cmbExtensions->itemText(i);
    }

    int item = ui->cmbExtensions->currentIndex();
    selectedfilters.removeAt(item);
    selectedfilters.removeDuplicates();
    selectedfilters.sort(Qt::CaseInsensitive);
    ui->cmbExtensions->clear();
    ui->cmbExtensions->addItems(selectedfilters);
    ui->cmbExtensions->setCurrentIndex(item);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    settings.setValue("selectedfilters", selectedfilters);
    settings.endGroup();
}
// Extension2
void TabWidget::defaultExtensions2()
{
    QMessageBox *msgBox = new QMessageBox(this);
    QAbstractButton *noButton = msgBox->addButton(tr("No"), QMessageBox::NoRole);
    QAbstractButton *yesButton = msgBox->addButton(tr("Yes"), QMessageBox::YesRole);
    msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("Do you want to delete all saved file extensions and restore the file extension to the default values?"));
//    msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
    msgBox->exec();

    if(msgBox->clickedButton() == noButton) {
        delete yesButton;
        delete msgBox;
        return;
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    settings.setValue("selectedextensions", Filters::allExtensions());
    settings.endGroup();
    ui->cmbExtensions_2->clear();
    ui->cmbExtensions_2->addItems(Filters::allExtensions());
    delete yesButton;
    delete msgBox;
}
void TabWidget::newExtensions2()
{
    QInputDialog *dialog = new QInputDialog(this);
    dialog->setInputMode(QInputDialog::TextInput);
    dialog->setCancelButtonText(tr("Cancel"));
    dialog->setOkButtonText(tr("OK"));
    dialog->setWindowTitle(DISPLAY_NAME " " VERSION);
    dialog->setLabelText(tr("The file name extension, without the preceding dot."));

    if(dialog->exec() == QDialog::Accepted) {
        QString text = dialog->textValue();
        // QRegExp rx("^[a-zA-Z0-9]+$");
        // if(!rx.exactMatch(text)) {
        static QRegularExpression rx("^[a-zA-Z0-9]+$");
        QRegularExpressionMatch m = rx.match(text);

        if(!m.hasMatch()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("Incorrect file name extension.\nPermitted characters are a-z, A-Z and 0-9."));
            return;
        }

        QStringList selectedextensions;
        ui->cmbExtensions_2->addItem(text);

        for(int i = 0; i < ui->cmbExtensions_2->count(); i++) {
            selectedextensions << ui->cmbExtensions_2->itemText(i);
        }

        selectedextensions.removeDuplicates();
        selectedextensions.sort(Qt::CaseInsensitive);
        ui->cmbExtensions_2->clear();
        ui->cmbExtensions_2->addItems(selectedextensions);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Extensions");
        settings.setValue("selectedextensions", selectedextensions);
        settings.endGroup();
    }
}

void TabWidget::removeExtensions2()
{
    QStringList selectedextensions;

    for(int i = 0; i < ui->cmbExtensions_2->count(); i++) {
        selectedextensions << ui->cmbExtensions_2->itemText(i);
    }

    int item = ui->cmbExtensions_2->currentIndex();
    selectedextensions.removeAt(item);
    selectedextensions.removeDuplicates();
    selectedextensions.sort(Qt::CaseInsensitive);
    ui->cmbExtensions_2->clear();
    ui->cmbExtensions_2->addItems(selectedextensions);
    ui->cmbExtensions_2->setCurrentIndex(item);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    settings.setValue("selectedextensions", selectedextensions);
    settings.endGroup();
}

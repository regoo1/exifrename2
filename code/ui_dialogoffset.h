/********************************************************************************
** Form generated from reading UI file 'dialogoffset.ui'
**
** Created by: Qt User Interface Compiler version 5.15.12
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGOFFSET_H
#define UI_DIALOGOFFSET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_DialogOffset
{
public:

    void setupUi(QDialog *DialogOffset)
    {
        if (DialogOffset->objectName().isEmpty())
            DialogOffset->setObjectName(QString::fromUtf8("DialogOffset"));
        DialogOffset->resize(400, 300);

        retranslateUi(DialogOffset);

        QMetaObject::connectSlotsByName(DialogOffset);
    } // setupUi

    void retranslateUi(QDialog *DialogOffset)
    {
        DialogOffset->setWindowTitle(QCoreApplication::translate("DialogOffset", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DialogOffset: public Ui_DialogOffset {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGOFFSET_H

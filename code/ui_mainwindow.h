/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.12
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
// #include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionClose;
    QAction *actionSettings;
    QAction *actionAbout;
    QAction *actionCheckForUpdates;
    QAction *actionUpdate;
    QAction *actionSvenska;
    QAction *actionEnglish;
    QAction *actionRename;
    QAction *actionExtensions;
    QAction *actionLicense;
    QAction *actionOpenCopy;
    QAction *actionCopy;
    QAction *actionMiscellaneous;
    QAction *actionIn;
    QAction *actionOut;
    QAction *actionDefault;
    QAction *actionDeleteAllSettings;
    QAction *actionRenameRecursively;
    QAction *actionClear;
    QAction *actionFont;
    QAction *actionChangeEXIF;
    QAction *actionChangeEXIFRename;
    QAction *actionEditEXIFOffset;
    QAction *actionEditEXIFOffsetRename;
    QWidget *centralwidget;
    QFormLayout *formLayout;
    QProgressBar *progressBar;
    QTextEdit *textBrowserMain;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuTools;
    QMenu *menuHelp;
    QMenu *menuLanguage;
    QMenu *menuView;
    QMenu *menuZoom;
    QMenu *menuEdit;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(900, 400);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/exifrename.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setAnimated(true);
        MainWindow->setDocumentMode(false);
        MainWindow->setDockNestingEnabled(false);
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        actionSettings = new QAction(MainWindow);
        actionSettings->setObjectName(QString::fromUtf8("actionSettings"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionCheckForUpdates = new QAction(MainWindow);
        actionCheckForUpdates->setObjectName(QString::fromUtf8("actionCheckForUpdates"));
        actionUpdate = new QAction(MainWindow);
        actionUpdate->setObjectName(QString::fromUtf8("actionUpdate"));
        actionUpdate->setEnabled(true);
        actionSvenska = new QAction(MainWindow);
        actionSvenska->setObjectName(QString::fromUtf8("actionSvenska"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/swedish.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSvenska->setIcon(icon1);
        actionEnglish = new QAction(MainWindow);
        actionEnglish->setObjectName(QString::fromUtf8("actionEnglish"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/english.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEnglish->setIcon(icon2);
        actionRename = new QAction(MainWindow);
        actionRename->setObjectName(QString::fromUtf8("actionRename"));
        actionExtensions = new QAction(MainWindow);
        actionExtensions->setObjectName(QString::fromUtf8("actionExtensions"));
        actionLicense = new QAction(MainWindow);
        actionLicense->setObjectName(QString::fromUtf8("actionLicense"));
        actionOpenCopy = new QAction(MainWindow);
        actionOpenCopy->setObjectName(QString::fromUtf8("actionOpenCopy"));
        actionCopy = new QAction(MainWindow);
        actionCopy->setObjectName(QString::fromUtf8("actionCopy"));
        actionMiscellaneous = new QAction(MainWindow);
        actionMiscellaneous->setObjectName(QString::fromUtf8("actionMiscellaneous"));
        actionIn = new QAction(MainWindow);
        actionIn->setObjectName(QString::fromUtf8("actionIn"));
        actionOut = new QAction(MainWindow);
        actionOut->setObjectName(QString::fromUtf8("actionOut"));
        actionDefault = new QAction(MainWindow);
        actionDefault->setObjectName(QString::fromUtf8("actionDefault"));
        actionDeleteAllSettings = new QAction(MainWindow);
        actionDeleteAllSettings->setObjectName(QString::fromUtf8("actionDeleteAllSettings"));
        actionRenameRecursively = new QAction(MainWindow);
        actionRenameRecursively->setObjectName(QString::fromUtf8("actionRenameRecursively"));
        actionClear = new QAction(MainWindow);
        actionClear->setObjectName(QString::fromUtf8("actionClear"));
        actionFont = new QAction(MainWindow);
        actionFont->setObjectName(QString::fromUtf8("actionFont"));
        actionChangeEXIF = new QAction(MainWindow);
        actionChangeEXIF->setObjectName(QString::fromUtf8("actionChangeEXIF"));
        actionChangeEXIFRename = new QAction(MainWindow);
        actionChangeEXIFRename->setObjectName(QString::fromUtf8("actionChangeEXIFRename"));
        actionEditEXIFOffset = new QAction(MainWindow);
        actionEditEXIFOffset->setObjectName(QString::fromUtf8("actionEditEXIFOffset"));
        actionEditEXIFOffsetRename = new QAction(MainWindow);
        actionEditEXIFOffsetRename->setObjectName(QString::fromUtf8("actionEditEXIFOffsetRename"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        formLayout = new QFormLayout(centralwidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        formLayout->setWidget(0, QFormLayout::FieldRole, progressBar);

        textBrowserMain = new QTextEdit(centralwidget);
        textBrowserMain->setObjectName(QString::fromUtf8("textBrowserMain"));
        textBrowserMain->setAcceptDrops(false);
        textBrowserMain->setAutoFillBackground(false);
        textBrowserMain->setFrameShape(QFrame::Box);
        textBrowserMain->setFrameShadow(QFrame::Raised);
        textBrowserMain->setLineWidth(1);
        textBrowserMain->setMidLineWidth(0);
        textBrowserMain->setUndoRedoEnabled(false);
        textBrowserMain->setReadOnly(true);
        textBrowserMain->setTabStopDistance(89.000000000000000);
        textBrowserMain->setAcceptRichText(false);
        textBrowserMain->setCursorWidth(0);
        textBrowserMain->setTextInteractionFlags(Qt::TextSelectableByMouse);

        formLayout->setWidget(1, QFormLayout::FieldRole, textBrowserMain);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 900, 22));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuTools = new QMenu(menubar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuLanguage = new QMenu(menubar);
        menuLanguage->setObjectName(QString::fromUtf8("menuLanguage"));
        menuView = new QMenu(menubar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuZoom = new QMenu(menuView);
        menuZoom->setObjectName(QString::fromUtf8("menuZoom"));
        menuEdit = new QMenu(menubar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuEdit->menuAction());
        menubar->addAction(menuTools->menuAction());
        menubar->addAction(menuView->menuAction());
        menubar->addAction(menuLanguage->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionRename);
        menuFile->addAction(actionRenameRecursively);
        menuFile->addAction(actionCopy);
        menuFile->addAction(actionClear);
        menuFile->addAction(actionClose);
        menuTools->addAction(actionSettings);
        menuTools->addAction(actionExtensions);
        menuTools->addAction(actionOpenCopy);
        menuTools->addAction(actionMiscellaneous);
        menuTools->addAction(actionFont);
        menuTools->addAction(actionUpdate);
        menuTools->addAction(actionDeleteAllSettings);
        menuHelp->addAction(actionCheckForUpdates);
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionLicense);
        menuLanguage->addAction(actionEnglish);
        menuLanguage->addAction(actionSvenska);
        menuView->addAction(menuZoom->menuAction());
        menuZoom->addAction(actionIn);
        menuZoom->addAction(actionDefault);
        menuZoom->addAction(actionOut);
        menuEdit->addAction(actionChangeEXIF);
        menuEdit->addAction(actionChangeEXIFRename);
        menuEdit->addAction(actionEditEXIFOffset);
        menuEdit->addAction(actionEditEXIFOffsetRename);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionClose->setText(QCoreApplication::translate("MainWindow", "Close", nullptr));
        actionSettings->setText(QCoreApplication::translate("MainWindow", "File name pattern...", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About...", nullptr));
        actionCheckForUpdates->setText(QCoreApplication::translate("MainWindow", "Check for updates", nullptr));
        actionUpdate->setText(QCoreApplication::translate("MainWindow", "Update", nullptr));
        actionSvenska->setText(QCoreApplication::translate("MainWindow", "Svenska", nullptr));
#if QT_CONFIG(statustip)
        actionSvenska->setStatusTip(QCoreApplication::translate("MainWindow", "Swedish", nullptr));
#endif // QT_CONFIG(statustip)
        actionEnglish->setText(QCoreApplication::translate("MainWindow", "English", nullptr));
#if QT_CONFIG(statustip)
        actionEnglish->setStatusTip(QCoreApplication::translate("MainWindow", "Engelska", nullptr));
#endif // QT_CONFIG(statustip)
        actionRename->setText(QCoreApplication::translate("MainWindow", "Rename...", nullptr));
#if QT_CONFIG(tooltip)
        actionRename->setToolTip(QCoreApplication::translate("MainWindow", "The file will be named after its EXIF Date/Time. If you\n"
"specified this, your selected text string will also be added\n"
" to the file name.", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        actionRename->setStatusTip(QCoreApplication::translate("MainWindow", "The file will be named after its EXIF Date/Time. If you specified this, your selected text string will also be added to the file name.", nullptr));
#endif // QT_CONFIG(statustip)
        actionExtensions->setText(QCoreApplication::translate("MainWindow", "Extensions...", nullptr));
        actionLicense->setText(QCoreApplication::translate("MainWindow", "License...", nullptr));
        actionOpenCopy->setText(QCoreApplication::translate("MainWindow", "Open / Copy to...", nullptr));
        actionCopy->setText(QCoreApplication::translate("MainWindow", "Copy and Rename...", nullptr));
#if QT_CONFIG(tooltip)
        actionCopy->setToolTip(QCoreApplication::translate("MainWindow", "The files you select will be copied to the location you selected.\n"
"Subfolders are created according to your settings.\n"
"The files are renamed according to your settings.", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        actionCopy->setStatusTip(QCoreApplication::translate("MainWindow", "The files you select will be copied to the location you selected. Subfolders are created according to your settings. The files are renamed according to your settings.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        actionCopy->setWhatsThis(QString());
#endif // QT_CONFIG(whatsthis)
        actionMiscellaneous->setText(QCoreApplication::translate("MainWindow", "Miscellaneous...", nullptr));
        actionIn->setText(QCoreApplication::translate("MainWindow", "In +", nullptr));
#if QT_CONFIG(statustip)
        actionIn->setStatusTip(QCoreApplication::translate("MainWindow", "Zoom with Ctrl + Mouse Wheel", nullptr));
#endif // QT_CONFIG(statustip)
        actionOut->setText(QCoreApplication::translate("MainWindow", "Out -", nullptr));
#if QT_CONFIG(statustip)
        actionOut->setStatusTip(QCoreApplication::translate("MainWindow", "Zoom with Ctrl + Mouse Wheel", nullptr));
#endif // QT_CONFIG(statustip)
        actionDefault->setText(QCoreApplication::translate("MainWindow", "Default", nullptr));
#if QT_CONFIG(statustip)
        actionDefault->setStatusTip(QCoreApplication::translate("MainWindow", "Zoom with Ctrl + Mouse Wheel", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteAllSettings->setText(QCoreApplication::translate("MainWindow", "Delete all settings...", nullptr));
        actionRenameRecursively->setText(QCoreApplication::translate("MainWindow", "Rename recursively...", nullptr));
#if QT_CONFIG(tooltip)
        actionRenameRecursively->setToolTip(QCoreApplication::translate("MainWindow", "All files in the folder you select and in all subfolders will be\n"
"renamed if they contain EXIF data. Each file will be renamed\n"
"according to its EXIF Date/Time. If you specified a text\n"
"string to be included in the file name, it will be added.", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        actionRenameRecursively->setStatusTip(QCoreApplication::translate("MainWindow", "All files in the folder you select and in all subfolders will be renamed if they contain EXIF data. Each file will be renamed according to its EXIF Date/Time. If you specified a text string to be included in the file name, it will be added.", nullptr));
#endif // QT_CONFIG(statustip)
        actionClear->setText(QCoreApplication::translate("MainWindow", "Clear", nullptr));
        actionFont->setText(QCoreApplication::translate("MainWindow", "Font...", nullptr));
        actionChangeEXIF->setText(QCoreApplication::translate("MainWindow", "EXIF Date/Time...", nullptr));
        actionChangeEXIFRename->setText(QCoreApplication::translate("MainWindow", "EXIF Date/Time and Rename...", nullptr));
        actionEditEXIFOffset->setText(QCoreApplication::translate("MainWindow", "EXIF Date/Time with offset...", nullptr));
        actionEditEXIFOffsetRename->setText(QCoreApplication::translate("MainWindow", "EXIF Date/Time with offset and Rename...", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "&File", nullptr));
        menuTools->setTitle(QCoreApplication::translate("MainWindow", "&Tools", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("MainWindow", "&Help", nullptr));
        menuLanguage->setTitle(QCoreApplication::translate("MainWindow", "&Language", nullptr));
        menuView->setTitle(QCoreApplication::translate("MainWindow", "View", nullptr));
#if QT_CONFIG(statustip)
        menuZoom->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
        menuZoom->setTitle(QCoreApplication::translate("MainWindow", "Zoom", nullptr));
        menuEdit->setTitle(QCoreApplication::translate("MainWindow", "&Edit", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

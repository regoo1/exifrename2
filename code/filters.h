//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef FILTERS_H
#define FILTERS_H

#include <QObject>
#include <QStringList>


class Filters : public QObject
{

    Q_OBJECT

public:
    explicit Filters(QObject *parent = nullptr);
    ~Filters()
    {
        delete this;
    }
    static QStringList allFilters()
    {
        QStringList  filters{
            tr("All Files") + " (*)",
            "jpeg [*.jpg *.jpeg] (*.jpg *.jpeg)",
            "Adobe [*.dng] (*.dng)",
            "Arri Alexa [*.ari] (*.ari)",
            "Blackmagic Design [*.braw] (*.braw)",
            "Canon [*.crw *.cr2 *.cr3] (*.crw *.cr2 *.cr3)",
            "Casio [*.bay] (*.bay)",
            "Cintel [*.cri] (*.cri)",
            "Epson [*.erf] (*.erf)",
            "Fuji [*.raf] (*.raf)",
            "GoPro [*.gpr] (*.gpr)",
            "Hasselblad [*.3fr] (*.3fr)",
            "Imacon/Hasselblad raw [*.fff] (*.fff)",
            "Kodak [*.dcs *.dcr *.drf *.k25 *.kdc] (*.dcs *.dcr *.drf *.k25 *.kdc)",
            "Leaf [*.mos] (*.mos)",
            "Leica [*.raw *.rwl *.dng] (*.raw *.rwl *.dng)",
            "Logitech [*.pxn] (*.pxn)",
            "Mamiya [*.mef] (*.mef)",
            "Minolta, Agfa [*.mdc] (*.mdc)",
            "Minolta, Konica Minolta [*.mrw] (*.mrw)",
            "Nikon [*.nef *.nrw] (*.nef *.nrw)",
            "Olympus [*.orf] (*.orf)",
            "Panasonic [*.raw *.rw2] (*.raw *.rw2)",
            "Pentax [*.pef *.ptx] (*.pef *.ptx)",
            "Phase One [*.cap *.iiq *.eip] (*.cap *.iiq *.eip)",
            "Rawzor [*.rwz] (*.rwz)",
            "RED Digital Cinema [*.R3D] (*.R3D)",
            "Samsung [*.srw] (*.srw)",
            "Sigma [*.x3f] (*.x3f)",
            "Sony [*.arw *.srf *.sr2] (*.arw *.srf *.sr2)"
        };
        filters.sort(Qt::CaseInsensitive);
        return filters;
    }


    static QStringList allExtensions()
    {
        QStringList  extensions{
            "3fr", "R3D", "ari", "arw", "bay", "braw", "cap",
            "cr2", "cr3", "cri", "crw", "dcr", "dcs", "dng",
            "drf", "eip", "erf", "fff", "gpr", "iiq", "jpeg",
            "jpg", "k25", "kdc", "mdc", "mef", "mos", "mrw", "nef",
            "nrw", "orf", "pef", "ptx", "pxn", "raf", "raw", "rw2",
            "rwl", "rwz", "sr2", "srf", "srw", "x3f"
        };
        extensions.sort(Qt::CaseInsensitive);
        return extensions;
    }
};

#endif // FILTERS_H

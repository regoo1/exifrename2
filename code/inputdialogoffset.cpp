//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "inputdialogoffset.h"
#include "info.h"
#include "qregularexpression.h"
#include <QDialogButtonBox>
// #include <QRegExpValidator>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QLabel>
#include <QFontDatabase>
// #include <QRegExpression>

InputDialogOffset::InputDialogOffset(QWidget *parent)
    : QDialog(parent)
{
    this->setAttribute(Qt::WA_QuitOnClose, true);
    this->setFixedSize(870, 270);
    QString fontPathRegular = ":/fonts/Ubuntu-R.ttf";
    this->setWindowTitle(DISPLAY_NAME " " VERSION);
    int fontidRegular = QFontDatabase::addApplicationFont(fontPathRegular);
    regular = QFontDatabase::applicationFontFamilies(fontidRegular).at(0);
    QFont fregular(regular, FONTSIZE + 2, QFont::Normal);
    QString iconpath = ":/images/exifrename.png";
    QIcon icon(iconpath);
    this->setFont(fregular);
    this->setWindowIcon(icon);
}

void InputDialogOffset::getOldDateTime(QString olddatetime)
{
    QString fontPathMono = ":/fonts/UbuntuMono-R.ttf";
    int fontidMono = QFontDatabase::addApplicationFont(fontPathMono);
    QString mono = QFontDatabase::applicationFontFamilies(fontidMono).at(0);
    QFont fmono(mono, FONTSIZE + 6);
    QVBoxLayout * vbox = new QVBoxLayout;
    vbox->addWidget(new QLabel(tr("Type in correct Date and Time. You must type exactly like this:") + "\n" + tr("Year-Month-day Hour:Minute:Second (4 digits for Year, 2 digits for Month, Day, Hour, Minute and Second)") + "\n" + tr("For example") + " " + olddatetime + " " + tr("(The image's current EXIF data for Date and Time.)")));
    le = new QLineEdit();
    le->setFont(fmono);
    lblInfo = new QLabel();
    lblInfo->setWordWrap(true);
    le->setText(olddatetime);
    vbox->addWidget(le);
    vbox->addWidget(lblInfo);
//    QRegExpValidator * validator = new QRegExpValidator(QRegExp("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}"));
//    le->setValidator(validator);
//    connect(validator, &QRegExpValidator::changed, []() {
//        qInfo() << "ANDRAS";
//    });
    le->setInputMask("9999-99-99 99:99:99");
    QPushButton *calculateButton = new QPushButton(tr("&Calculate"));
//    connect(calculateButton, &QPushButton::pressed, [this, validator]() {
//        le->setValidator(validator);
//    });
    calculateButton->setDefault(true);
    okButton = new QPushButton(tr("&OK"));
    okButton->setDisabled(true);
    QPushButton *cancelButton = new QPushButton(tr("&Cancel"));
    QDialogButtonBox *buttonBox = new QDialogButtonBox;
    buttonBox->addButton(calculateButton, QDialogButtonBox::ActionRole);
    buttonBox->addButton(okButton, QDialogButtonBox::ActionRole);
    buttonBox->addButton(cancelButton, QDialogButtonBox::ActionRole);
    vbox->addWidget(buttonBox);
    this->setLayout(vbox);
    le->setEnabled(true);
    connect(calculateButton, &QPushButton::clicked, [olddatetime, this]() {
        getNewValue(olddatetime);
    });
    connect(okButton, &QPushButton::clicked, [this]() {
        close();
        emit setOffset(OFFSET);
    });
    connect(cancelButton, &QPushButton::clicked, this, &InputDialogOffset::close);
}

InputDialogOffset::~InputDialogOffset()
{
}

void InputDialogOffset::getNewValue(QString olddatetime)
{
    QString nydatetime = le->text();
    static QRegularExpression rx("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}");;
    QRegularExpressionMatch m = rx.match(nydatetime);

    if(!m.hasMatch()) {
        lblInfo->setText(tr("Not correct. Four digits for year. Two digits for month, day, hour, minute and second."));
        return;
    }

    QString ar = nydatetime.mid(0, 4);
    QString manad = nydatetime.mid(5, 2);
    QString dag = nydatetime.mid(8, 2);
    QString tim = nydatetime.mid(11, 2);
    QString min = nydatetime.mid(14, 2);
    QString sek = nydatetime.mid(17, 2);
    bool isvaliddate = QDate::isValid(ar.toInt(), manad.toInt(), dag.toInt());
    bool isvalidtime = QTime::isValid(tim.toInt(), min.toInt(), sek.toInt(), 0);

    if(!isvaliddate && !isvalidtime) {
        lblInfo->setText(tr("This date and this time does not exist."));
        return;
    }

    if(!isvaliddate) {
        lblInfo->setText(tr("This date does not exist."));
        return;
    }

    if(!isvalidtime) {
        lblInfo->setText(tr("This time does not exist."));
        return;
    }

    QString format = "yyyy-MM-dd HH:mm:ss";
    QDateTime old_dt = QDateTime::fromString(olddatetime, format);
    QDateTime new_dt = QDateTime::fromString(nydatetime, format);
    qint64 secondsDiff = old_dt.secsTo(new_dt);
    QString sdt = QString::number(secondsDiff);
    OFFSET = sdt;
    okButton->setEnabled(true);
    lblInfo->setText(tr("Offset calculated to") + " " + sdt + " " + tr("seconds. This offset is used on the images then you choose to change EXIF Date and Time."));
}

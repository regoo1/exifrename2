//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "info.h"

QString Info::getSystem()
{
    const QString syfte = "<h3 style=\"color:green;\">" +
                          tr("A program to name pictures after the date and time "
                             "the picture was taken.") +
                          "</h3>";
    const QString translator = "";
    /*  */
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();
    QString applicationHomepage;

    if(sp == "sv_SE") {
        applicationHomepage = APPLICATION_HOMEPAGE;
    } else {
        applicationHomepage = APPLICATION_HOMEPAGE_ENG;
    }

    /* */
    // STOP EDIT
    QString v = "";
    v += QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + " (" +
         QSysInfo::currentCpuArchitecture() + ").\n";
    v += DISPLAY_NAME " " VERSION + tr(" was created ") + BUILD_DATE_TIME "\n" +
         tr("by a computer with") + " " + QSysInfo::buildAbi() + ".";
    // write GCC version
    QString temp;
#if defined _MSC_VER || __GNUC__ || defined __clang__ || defined __ICC ||      \
    defined __clang__
#if defined __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined __GNUC__ && !defined __clang__
#if defined __GNUC__ && !defined __MINGW32__
#define COMPILER "GCC"
#endif
#ifdef __MINGW32__
#define COMPILER "MinGW GCC"
#endif
    temp = (QString(tr(" Compiled by") + " %1 %2.%3.%4%5")
            .arg(COMPILER)
            .arg(__GNUC__)
            .arg(__GNUC_MINOR__)
            .arg(__GNUC_PATCHLEVEL__)
            .arg("."));
#endif
#ifdef __ICC
#define INTEL "Intel icl"
    temp = (QString(tr(" Compiled by") + " %1 %2 Build date: %3 Update: %4%5")
            .arg(INTEL)
            .arg(__INTEL_COMPILER)
            .arg(__INTEL_COMPILER_BUILD_DATE)
            .arg(__INTEL_COMPILER_UPDATE)
            .arg("."));
#endif
    v += temp;
#if defined __clang__
    v += (QString(tr(" Compiled by") + " %1 %2.%3.%4%5")
          .arg("Clang")
          .arg(__clang_major__)
          .arg(__clang_minor__)
          .arg(__clang_patchlevel__)
          .arg("."));
#endif
#endif
// MSVC version
#ifdef _MSC_VER
#define COMPILER "MSVC++"
    QString tmp = QString::number(_MSC_VER);
    QString version;
    QString full_version =
        tr("Full version number ") + QString::number(_MSC_FULL_VER);

    switch(_MSC_VER) {
        case 1500:
            version = "9.0 (Visual Studio 2008)\n" + full_version;
            break;

        case 1600:
            version = "10.0 (Visual Studio 2010)\n" + full_version;
            break;

        case 1700:
            version = "11.0 (Visual Studio 2012)<br>" + full_version;
            break;

        case 1800:
            version = "12.0 (Visual Studio 2013)\n" + full_version;
            break;

        case 1900:
            version = "14.0 (Visual Studio 2015)\n" + full_version;
            break;

        case 1910:
            version = "15.0 (Visual Studio 2017)\n" + full_version;
            break;

        case 1912:
            version = "15.5 (Visual Studio 2017)\n" + full_version;
            break;

        case 1914:
            version = "15.7 (Visual Studio 2017)\n" + full_version;
            break;

        case 1915:
            version = "15.8 (Visual Studio 2017)\n" + full_version;
            break;

        case 1916:
            version = "15.9 (Visual Studio 2017)\n" + full_version;
            break;

        case 1921:
            version = "16.1 (Visual Studio 2019)\n" + full_version;
            break;

        case 1922:
            version = "16.2 (Visual Studio 2019)\n" + full_version;
            break;

        case 1923:
            version = "16.3 (Visual Studio 2019)\n" + full_version;
            break;

        case 1924:
            version = "16.4 (Visual Studio 2019)\n" + full_version;
            break;

        case 1925:
            version = "16.5 (Visual Studio 2019)\n" + full_version;
            break;

        case 1926:
            version = "16.6 (Visual Studio 2019)\n" + full_version;
            break;

        case 1927:
            version = "16.7 (Visual Studio 2019)\n" + full_version;
            break;

        case 1928:
            version = "16.8 (Visual Studio 2019)\n" + full_version;
            break;

        default:
            version = tr("Unknown version") + "\n" + full_version;
            break;
    }

    v += (QString(tr(" Compiled by") + " %1 %2%3")
          .arg(COMPILER)
          .arg(version)
          .arg("."));
#endif
    v += "\n";
#else
    v += tr("Unknown compiler.");
    v += "\n";
#endif
//    v += "<br>";
    QString CURRENT_YEAR_S = CURRENT_YEAR;
    CURRENT_YEAR_S = CURRENT_YEAR_S.right(4);
#if __cplusplus==201703L
    QString compiled = "C++17";
#elif __cplusplus==201402L
    QString compiled = "C++14";
#elif __cplusplus==201103L
    QString compiled = "C++11";
#else
    QString compiled = "C++";
#endif
    v += "Revision " + compiled + ".";
    const QString auther =
        "<h3 style=\"color:green;\">Copyright &copy; " START_YEAR " - " +
        CURRENT_YEAR_S +
        " " PROGRAMMER_NAME "</h3><br><a style=\"text-decoration:none;\" href='" +
        applicationHomepage + "'>" + tr("Home page") +
        "</a> | <a style=\"text-decoration:none;\" href='" SOURCEKODE "'>" +
        tr("Source code") +
        "</a> | <a style=\"text-decoration:none;\" href='" WIKI "'> " +
        tr("Wiki") +
        "</a> | <a style=\"text-decoration:none;\" href='" DOWNLOAD_PATH "'>" +
        tr("Downloads") +
        "</a><br><a style=\"text-decoration:none;\"  "
        "href=\"mailto:" PROGRAMMER_EMAIL "\">" PROGRAMMER_EMAIL "</a>  <br>" +
        tr("Phone: ") + PROGRAMMER_PHONE;
    QString about = "<h1 style=\"color:green;\">" DISPLAY_NAME " " VERSION
                    "</h1>" +
                    syfte + auther + translator + "<br>" + "<br>" +
                    tr("This program uses Qt version ") + QT_VERSION_STR +
                    tr(" running on ") + v;
//            + "<br><a style=\"text-decoration:none;\" href=\"" LICENSE "\">" + license +  "</a>";
    return about;
}

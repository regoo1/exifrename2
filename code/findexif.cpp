//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "qregularexpression.h"
#include "tabwidget.h"
#include "exif.h"
#include "ui_mainwindow.h"
#include <fstream>

void MainWindow::findExif(QList<QUrl> urls)
{
    QStringList localurls;

    foreach(QUrl u, urls) {
        localurls << QString(u.toLocalFile());
    }

    findExif(localurls);
}

void MainWindow::findExif(QStringList urls)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Tools");
    bool displayfailure = settings.value("displayfailure", true).toBool();
    bool checkredtext = settings.value("checkredtext", false).toBool();
    settings.endGroup();
    settings.beginGroup("Extensions");
    QStringList selectedextensions = settings.value("selectedextensions").toStringList();
    settings.endGroup();
    QStringList tempurls;
    int antal = urls.count();
    ui->progressBar->setMaximum(antal);
    ui->progressBar->setValue(0);
    /*  */

    /*  */
    foreach(QString s, urls) {
        QFileInfo fi(s);

        if(selectedextensions.contains(fi.suffix(), Qt::CaseInsensitive)) {
            if(fi.exists() && fi.isWritable() && fi.isFile()) {
                tempurls << s;
//                if(displaysuccess) {
//                    ui->textBrowserMain->append(tr("Correct file name extension.") + " \"" + QDir::toNativeSeparators(s) + "\"");
//                }
            }
        } else {
            if(displayfailure) {
                if(checkredtext) {
                    ui->textBrowserMain->setTextColor(QColor("red"));
                    ui->textBrowserMain->append(tr("Incorrect file name extension.") + " \"" + QDir::toNativeSeparators(s) + "\"");
                    ui->textBrowserMain->setTextColor(QColor("black"));
                } else {
                    ui->textBrowserMain->append(tr("Incorrect file name extension.") + " \"" + QDir::toNativeSeparators(s) + "\"");
                }
            }

            antal--;
        }
    }

    urls.clear();
    urls = tempurls;
    Exif exif;
    int failed = 0;

    if(antal == 1) {
        ui->textBrowserMain->append(tr("Trying to rename") + " " + QString::number(antal) + " " + tr("file"));
    } else {
        ui->textBrowserMain->append(tr("Trying to rename") + " " + QString::number(antal) + " " + tr("files"));
    }

    foreach(QString qfilename, urls) {
        char *hittat = new char[20];
        QString *info = new QString;
        bool b = exif.getExif(qfilename, hittat, info);
        QString testfile;
        testfile = QString::fromLatin1(hittat);
        // QRegExp rx("[0-9]{4}x[0-9]{2}x[0-9]{2}x[0-9]{2}x[0-9]{2}x[0-9]{2}");
        // if(!rx.exactMatch(testfile)) {
        static QRegularExpression rx("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}");
        QRegularExpressionMatch m = rx.match(testfile);

        if(!m.hasMatch()) {
            b = false;
        }

        if(b) {
            emit filKlar(ui->progressBar->value() + 1);
            QString name = QString(QString::fromLatin1(hittat));
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            settings.setIniCodec("UTF-8");
#endif
            settings.beginGroup("PathPattern");
            bool onlytimeinfilename = settings.value("onlytimeinfilename", false).toBool();
            settings.endGroup();

            if(onlytimeinfilename) {
                name = name.right(8);
            }

            if(reName(qfilename, name, false)) {
            } else {
                failed++;
            }
        } else {
            if(displayfailure) {
                if(checkredtext) {
                    ui->textBrowserMain->setTextColor(QColor("red"));
                    ui->textBrowserMain->append(*info);
                    ui->textBrowserMain->setTextColor(QColor("black"));
                } else {
                    ui->textBrowserMain->append(*info);
                }
            }

            failed++;
        }
    }

    QString ratt, fel;

    if(antal > 0) {
        if(failed == 0) {
            ui->textBrowserMain->append(tr("Managed to rename all files"));
        } else if((antal - failed) == 0) {
            if(checkredtext) {
                ui->textBrowserMain->setTextColor(QColor("red"));
                ui->textBrowserMain->append(tr("Failed to rename any file"));
                ui->textBrowserMain->setTextColor(QColor("black"));
            } else {
                ui->textBrowserMain->append(tr("Failed to rename any file"));
            }
        } else {
            if((failed) == 1) {
                fel = tr("file");
            } else {
                fel = tr("files");
            }

            if((antal - failed) == 1) {
                ratt = tr("file");
            } else {
                ratt = tr("files");
            }

            if(checkredtext) {
                ui->textBrowserMain->setTextColor(QColor("red"));
                ui->textBrowserMain->append(tr("Failed to rename") + " " + QString::number(failed) + " " + fel + " " + tr("and managed to rename") + " " + QString::number(antal - failed) + " " + ratt);
                ui->textBrowserMain->setTextColor(QColor("black"));
            } else {
                ui->textBrowserMain->append(tr("Failed to rename") + " " + QString::number(failed) + " " + fel + " " + tr("and managed to rename") + " " + QString::number(antal - failed) + " " + ratt);
            }
        }
    }

    ui->textBrowserMain->append("");
}

/********************************************************************************
** Form generated from reading UI file 'selectfont.ui'
**
** Created by: Qt User Interface Compiler version 5.15.12
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTFONT_H
#define UI_SELECTFONT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectFont
{
public:
    QComboBox *comboFontSizes;
    QFontComboBox *comboFontFamilies;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QRadioButton *normal;
    QRadioButton *bold;
    QRadioButton *italic;
    QRadioButton *boldItalic;
    QComboBox *comboFilter;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pbDefault;
    QPushButton *pbExit;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *SelectFont)
    {
        if (SelectFont->objectName().isEmpty())
            SelectFont->setObjectName(QString::fromUtf8("SelectFont"));
        SelectFont->resize(671, 140);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SelectFont->sizePolicy().hasHeightForWidth());
        SelectFont->setSizePolicy(sizePolicy);
        SelectFont->setMinimumSize(QSize(2, 0));
        comboFontSizes = new QComboBox(SelectFont);
        comboFontSizes->setObjectName(QString::fromUtf8("comboFontSizes"));
        comboFontSizes->setGeometry(QRect(10, 10, 111, 31));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboFontSizes->sizePolicy().hasHeightForWidth());
        comboFontSizes->setSizePolicy(sizePolicy1);
        comboFontFamilies = new QFontComboBox(SelectFont);
        comboFontFamilies->setObjectName(QString::fromUtf8("comboFontFamilies"));
        comboFontFamilies->setGeometry(QRect(140, 10, 521, 31));
        sizePolicy.setHeightForWidth(comboFontFamilies->sizePolicy().hasHeightForWidth());
        comboFontFamilies->setSizePolicy(sizePolicy);
        comboFontFamilies->setBaseSize(QSize(0, 0));
        comboFontFamilies->setInsertPolicy(QComboBox::InsertAfterCurrent);
        comboFontFamilies->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
        layoutWidget = new QWidget(SelectFont);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 50, 371, 31));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        normal = new QRadioButton(layoutWidget);
        normal->setObjectName(QString::fromUtf8("normal"));
        normal->setChecked(true);

        horizontalLayout->addWidget(normal);

        bold = new QRadioButton(layoutWidget);
        bold->setObjectName(QString::fromUtf8("bold"));

        horizontalLayout->addWidget(bold);

        italic = new QRadioButton(layoutWidget);
        italic->setObjectName(QString::fromUtf8("italic"));

        horizontalLayout->addWidget(italic);

        boldItalic = new QRadioButton(layoutWidget);
        boldItalic->setObjectName(QString::fromUtf8("boldItalic"));

        horizontalLayout->addWidget(boldItalic);

        comboFilter = new QComboBox(SelectFont);
        comboFilter->setObjectName(QString::fromUtf8("comboFilter"));
        comboFilter->setGeometry(QRect(390, 50, 271, 31));
        layoutWidget1 = new QWidget(SelectFont);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 90, 651, 41));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        pbDefault = new QPushButton(layoutWidget1);
        pbDefault->setObjectName(QString::fromUtf8("pbDefault"));

        horizontalLayout_2->addWidget(pbDefault);

        pbExit = new QPushButton(layoutWidget1);
        pbExit->setObjectName(QString::fromUtf8("pbExit"));

        horizontalLayout_2->addWidget(pbExit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        retranslateUi(SelectFont);

        pbDefault->setDefault(false);


        QMetaObject::connectSlotsByName(SelectFont);
    } // setupUi

    void retranslateUi(QDialog *SelectFont)
    {
        SelectFont->setWindowTitle(QCoreApplication::translate("SelectFont", "Select font", nullptr));
        normal->setText(QCoreApplication::translate("SelectFont", "Normal", nullptr));
        bold->setText(QCoreApplication::translate("SelectFont", "Bold", nullptr));
        italic->setText(QCoreApplication::translate("SelectFont", "Italic", nullptr));
        boldItalic->setText(QCoreApplication::translate("SelectFont", "Bold and italic", nullptr));
        pbDefault->setText(QCoreApplication::translate("SelectFont", "Default", nullptr));
        pbExit->setText(QCoreApplication::translate("SelectFont", "Exit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SelectFont: public Ui_SelectFont {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTFONT_H

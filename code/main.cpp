//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"

#include <QApplication>
#include <QFontDatabase>
#include <QTranslator>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString fontPathRegular = ":/fonts/Ubuntu-R.ttf";
    int fontidRegular = QFontDatabase::addApplicationFont(fontPathRegular);
    QString regular = QFontDatabase::applicationFontFamilies(fontidRegular).at(0);
    QFont fregular(regular, FONTSIZE, QFont::Normal);
    QApplication::setFont(fregular);
    QTranslator translator;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Language");
    QString testlanguage = settings.value("testlanguage", "empty").toString();
    QString sp = settings.value("language", "").toString();
    settings.endGroup();
    /* */
    QFileInfo fi(testlanguage);

    if((testlanguage != "empty") && (fi.exists())) {
        if(translator.load(testlanguage)) {
            QApplication::installTranslator(&translator);
            settings.beginGroup("Language");
            settings.setValue("testlanguage", "empty");
            settings.endGroup();
        }
    }
    // END Test custum translation
    else {
        /*  */
        const QString translationPath = ":/i18n/_complete";

        if(sp == "") {
            sp = QLocale::system().name();

            if(translator.load(translationPath + "_" + sp + ".qm")) {
                QApplication::installTranslator(&translator);
                settings.beginGroup("Language");
                settings.setValue("language", sp);
                settings.endGroup();
            } else {
                if(sp != "en_US")
                    QMessageBox::information(
                        nullptr, DISPLAY_NAME " " VERSION,
                        DISPLAY_NAME
                        " is unfortunately not yet translated into your language, the "
                        "program will start with English menus.");

                settings.beginGroup("Language");
                settings.setValue("language", sp);
                settings.endGroup();
            }
        } else {
            if(translator.load(translationPath + "_" + sp + ".qm"))
                QApplication::installTranslator(&translator);
        }
    }

    MainWindow *w = new MainWindow;
    QObject::connect(&a, &QApplication::aboutToQuit,
                     [w]() -> void { w->setEndConfig(); });
    QObject::connect(&a, &QApplication::aboutToQuit, w,  &MainWindow::setEndConfig);
    w->show();
    return a.exec();
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "tabwidget.h"
#include <QGuiApplication>
#include <QScreen>
void TabWidget::readPlaceonscreen()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("TabWindow");
    QPoint punkt = settings.value("pos", QPoint(200, 200)).toPoint();
    this->move(settings.value("pos", QPoint(200, 200)).toPoint());
    int widd = 0, hojd = 0;

    foreach(QScreen *screen, QGuiApplication::screens()) {
        widd = screen->size().width();
        hojd = screen->size().height();

        if((punkt.x() > widd - 100) || (punkt.y() > hojd - 100) || punkt.x() < 0 ||
                punkt.y() < 0) {
            this->move(200, 200);
            break;
        }
    }

    settings.endGroup();
}
void TabWidget::writePlaceonscreen()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("TabWindow");
//  settings.setValue("tabsize", this->size());
    settings.setValue("pos", this->pos());
    settings.endGroup();
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "tabwidget.h"
#include "ui_tabwidget.h"

void TabWidget::setBackgroundColor()
{
    QColorDialog *colordialog = new QColorDialog;

    if(colordialog->exec()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Color");
        settings.setValue("backgroundcolor", colordialog->currentColor());
        settings.endGroup();
    }

    delete colordialog;
}
void TabWidget::setTextColor()
{
    QColorDialog *colordialog = new QColorDialog;

    if(colordialog->exec()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Color");
        settings.setValue("textcolor", colordialog->currentColor());
        settings.endGroup();
    }

    delete colordialog;
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "exif.h"
#include "exifw.h"
#include "mainwindow.h"
#include "qregularexpression.h"
#include "tabwidget.h"
#include "ui_mainwindow.h"
#include "inputdialog.h"

void MainWindow::changeExif(bool dorename)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    QStringList selectedfilters = settings.value("selectedfilters").toStringList();
    selectedfilters.sort(Qt::CaseInsensitive);
    settings.endGroup();
    QDir dir(QDir::home());
    QString home = QDir::toNativeSeparators(dir.absolutePath());
    settings.beginGroup("Path");
    QString openpath = settings.value("openpath", home).toString();
    settings.endGroup();
    QFileDialog * dialog = new QFileDialog(this);
    dialog->resize(900, 600);
    dialog->setViewMode(QFileDialog::Detail);
    dialog->setFileMode(QFileDialog::ExistingFile);
    dialog->setWindowTitle(tr("Select file to get new EXIF Date/Time"));
    settings.beginGroup("Tools");
    dialog->setOption(QFileDialog::DontUseNativeDialog, !settings.value("usenativedialog").toBool());
    bool useopenpath = settings.value("useopenpath", false).toBool();
    bool checkredtext = settings.value("checkredtext", false).toBool();
    settings.endGroup();
    settings.beginGroup("Extensions");
    QString selectednamefilter = settings.value("selectednamefilter", "tomt").toString();
    settings.endGroup();

    if(useopenpath) {
        if(!TabWidget::pathExist(openpath)) {
            dialog->setDirectory(home);
        } else if(openpath.isEmpty()) {
            dialog->setDirectory(home);
        } else {
            dialog->setDirectory(openpath);
        }
    } else {
        dialog->setDirectory(home);
    }

    QStringList fileNames;

    if(selectednamefilter != "tomt") {
        dialog->setNameFilters(QStringList() << selectednamefilter << selectedfilters);
    } else {
        dialog->setNameFilters(selectedfilters);
    }

    if(dialog->exec()) {
        fileNames = dialog->selectedFiles();
        qfilename = fileNames.at(0);
        char *hittat = new char[20];
        QString *info = new QString;
        Exif exif;
        bool b = exif.getExif(qfilename, hittat, info);
        QString qhittat;

        if(b) {
            qhittat = QString::fromLatin1(hittat);
            QRegularExpression rx("[0-9]{4}x[0-9]{2}x[0-9]{2}x[0-9]{2}x[0-9]{2}x[0-9]{2}");

            if(rx.isValid()) {
                if(checkredtext) {
                    ui->textBrowserMain->setTextColor(QColor("red"));
                    ui->textBrowserMain->append(tr("No EXIF data found in") + " " +  " \"" + QDir::toNativeSeparators(qfilename) + "\"");
                    ui->textBrowserMain->setTextColor(QColor("black"));
                } else {
                    ui->textBrowserMain->append(tr("No EXIF data found in") + " " +  " \"" + QDir::toNativeSeparators(qfilename) + "\"");
                }

                ui->textBrowserMain->append("");
                b = false;
            }
        } else {
            if(checkredtext) {
                ui->textBrowserMain->setTextColor(QColor("red"));
                ui->textBrowserMain->append(tr("No EXIF data found in") + " \"" +  QDir::toNativeSeparators(qfilename) + "\"");
                ui->textBrowserMain->setTextColor(QColor("black"));
            } else {
                ui->textBrowserMain->append(tr("No EXIF data found in") + " \"" +  QDir::toNativeSeparators(qfilename) + "\"");
            }

            ui->textBrowserMain->append("");
        }

        if(b) {
            ui->textBrowserMain->append(*info);
            qhittat.replace(4, 1, '-');
            qhittat.replace(7, 1, '-');
            qhittat.replace(10, 1, ' ');
            qhittat.replace(13, 1, ':');
            qhittat.replace(16, 1, ':');
            InputDialog dialog;
            dialog.getOldDateTime(qhittat, dorename);
            connect(&dialog, &InputDialog::newDateTime, this, &MainWindow::getNewDateTime);
            dialog.exec();
        }

        QDir dir = dialog->directory();
        openpath = dir.path();
        settings.beginGroup("Path");
        settings.setValue("openpath", openpath);
        settings.endGroup();
        settings.beginGroup("Extensions");
        settings.setValue("selectednamefilter", dialog->selectedNameFilter());
        settings.endGroup();
    }
}

void MainWindow::getNewDateTime(QString newdatetime, bool dorename)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Tools");
    bool checkredtext = settings.value("checkredtext", false).toBool();
    settings.endGroup();
    QString displaydatetime = newdatetime;
    string str_filename = qfilename.toStdString();
    char *cchar_filename = new char[str_filename.size()];
    strcpy(cchar_filename, str_filename.c_str());
    /* */
    newdatetime.remove(QChar('-'), Qt::CaseInsensitive);
    newdatetime.remove(QChar(':'), Qt::CaseInsensitive);
    newdatetime.remove(QChar(' '), Qt::CaseInsensitive);
    string str_newdatetime = newdatetime.toStdString();
    char *cchar_newdatetime = new char[str_newdatetime.size()];
    strcpy(cchar_newdatetime, str_newdatetime.c_str());
    /* */
    char *info = new char[50];
    Exifw exifw;
    bool exifChanged = exifw.setExif(cchar_filename, cchar_newdatetime, info);
    std::string tmp = (char*)info;
    QString qinfo = QString::fromStdString(tmp);

    if(exifChanged) {
        ui->textBrowserMain->append(qinfo + " \"" +  QDir::toNativeSeparators(qfilename) + "\" " + tr("changed to") + " \"" + displaydatetime + "\"");

        if(!dorename) {
            ui->textBrowserMain->append("");
        }

        /* Rename */
        if(dorename) {
            QString rename = displaydatetime;
            rename.replace(' ', 'x');
            rename.replace('-', 'x');
            rename.replace(':', 'x');
            reName(qfilename, rename, true);
        } // dorename

        /* END Rename */
    } else {
        if(checkredtext) {
            ui->textBrowserMain->setTextColor(QColor("red"));
            ui->textBrowserMain->append(qinfo + " \"" +  QDir::toNativeSeparators(qfilename) + "\"");
            ui->textBrowserMain->setTextColor(QColor("black"));
        } else {
            ui->textBrowserMain->append(qinfo + " \"" +  QDir::toNativeSeparators(qfilename) + "\"");
        }

        ui->textBrowserMain->append("");
    }
}

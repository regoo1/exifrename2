//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QGuiApplication>
/* draganddrop */
#include <QDebug>
#include <QDir>
#include <QDropEvent>
#include <QFontDatabase>
#include <QMessageBox>
#include <QMimeData>
#include <QProcess>
#include <QToolBox>
#include <QUrl>
#include <QFileDialog>
#include <QAbstractButton>
#include <QPushButton>
#include <QDirIterator>
#include <QStandardPaths>
#include <QStyle>
#include <QFileDevice>
#include <QScreen>
#include <QFontDialog>
// #include <QRegExpValidator>
#include <QLayout>
#include <QTextCursor>
//#include <QWheelEvent>
#include <QToolTip>
#include <QMessageBox>
#include "exif.h"
#include "info.h"
#include "tabwidget.h"
#include "filters.h"
// Check for Updates
#include "checkupdate.h"
// Dynamic build
// #include "checkforupdates_global.h"
// Update
#ifdef Q_OS_LINUX
#include "update.h"
#include "updatedialog.h"
#endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class TabWidget;
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    /* draganddrop */
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);
//    void wheelEvent(QWheelEvent *ev);

private:
    Ui::MainWindow *ui;
    void findExif(QStringList urls);
    void findExif(QList<QUrl> urls);
    bool reName(QString &path, QString &name, bool dorename);
    void readPlaceonscreen();
    void writePlaceonscreen();
    void checkOnStart();
    void doCheckForUpdates();
    QString UpdateInstructions();
    void connectLanguage();
    void firstRun();
    void zoom();
    void updateNeeded(bool needed);
    void openCopy();
    void copy(QStringList filetocopy);
    void copy(QList<QUrl> urls);
    QString qfilename;
    QStringList changeExifManyFiles(QString offset);

private slots:
    void getCloseSignalTabWidget();
    void isUpdated(bool b);
    void openFile();
    void openFileRecursively();
    void changeExif(bool rename);
    void getNewDateTime(QString newdatetime, bool rename);
    void textBrowserMaintextChanged();
    void zoomPlus();
    void zoomMinus();
    void zoomDefault();
    void getFont(QFont font);
    void selectFontClose();
    void changeExifOffset();
    void changeExifOffsetManyfilesRename();

#ifdef Q_OS_LINUX
    void timeToClose();
#endif

public slots:
    void setEndConfig();
    void getOffset(QString);
    void getOffsetRename(QString offset);

signals:

    void filKlar(int progress);
};
#endif // MAINWINDOW_H

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "tabwidget.h"
#include "checkupdate.h"
#include "checkupdate_global.h"
#include "info.h"
#ifdef Q_OS_LINUX // Linux
#include "update.h"
//#include "updatedialog.h"
#endif // Linux
#include "ui_tabwidget.h"

void TabWidget::testTranslation()
{
    QObject::connect(ui->pbTestLanguagefile, &QPushButton::clicked, [this]() {
        qInfo() << "HIT";
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Language");
        settings.setValue("testlanguage", "test");
        settings.endGroup();
        QFileDialog dialog(this,
                           tr("Open your language file"), QDir::homePath(), tr("Compiled language file (*.qm)"));

        if(!dialog.exec()) {
            return;
        }

        QMessageBox::information(
            nullptr, DISPLAY_NAME " " VERSION,
            tr("Shut down and restart") + " " DISPLAY_NAME " " + tr("and your selected language file will be loaded."));
        settings.beginGroup("Language");
        settings.setValue("testlanguage", dialog.selectedFiles().at(0));
        settings.endGroup();
//        const QString EXECUTE = QDir::toNativeSeparators(
//                                    QCoreApplication::applicationDirPath() + "/" +
//                                    QFileInfo(QCoreApplication::applicationFilePath()).fileName());
//        QProcess p;
//        p.setProgram(EXECUTE);
//        p.startDetached();
        close();
    });
}

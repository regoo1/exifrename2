//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "selectfont.h"
#include "ui_selectfont.h"

SelectFont::SelectFont(QWidget *parent)
    : QDialog(parent), ui(new Ui::SelectFont)
{
    this->setAttribute(Qt::WA_DeleteOnClose);
    ui->setupUi(this);
    QString iconpath = ":/images/exifrename.png";
    QIcon icon(iconpath);
    this->setWindowIcon(icon);
    // this->setWindowTitle(DISPLAY_NAME " " VERSION " " + tr("Select Font"));
    this->setFixedSize(THIS_WIDHT, THIS_HEIGHT);
    int currentfontsize = this->font().pointSize();
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
    QString familj = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont defaultfont(familj);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Font");
    QString family = settings.value("family", familj).toString();
    int size = settings.value("size", currentfontsize).toInt();
    bool bold = settings.value("bold", false).toBool();
    bool italic = settings.value("italic", false).toBool();
    bool boldItalic = settings.value("boldItalic", false).toBool();
    settings.endGroup();

    if(boldItalic) {
        ui->boldItalic->setChecked(true);
    } else if(bold) {
        ui->bold->setChecked(true);
    } else if(italic) {
        ui->italic->setChecked(true);
    } else {
        ui->normal->setChecked(true);
    }

    ui->comboFontFamilies->setCurrentFont(family);
    /* comboFilter */
    QStringList filter = {tr("All fonts"), tr("Monospaced fonts"),
                          tr("Proportional fonts")
                         };
    QList<QFontComboBox::FontFilter> qfilter = {QFontComboBox::AllFonts,
                                                QFontComboBox::MonospacedFonts,
                                                QFontComboBox::ProportionalFonts
                                               };
    ui->comboFilter->addItems(filter);
    connect(
        ui->comboFilter,
        static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    [this, qfilter, defaultfont]() -> void {
        ui->comboFontFamilies->clear();
        ui->comboFontFamilies->setFontFilters(
            qfilter.at(ui->comboFilter->currentIndex()));
        ui->comboFontFamilies->setCurrentFont(defaultfont.family());
        triggerSignal();
    });
    QStringList fontSizes = {"6",  "7",  "8",  "9",  "10", "11", "12", "13", "14",
                             "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
                             "27", "28", "29", "30", "31", "32", "33", "34"
                            };
    ui->comboFontSizes->addItems(fontSizes);
    ui->comboFontSizes->setCurrentText(QString::number(size));
    connect(ui->comboFontFamilies, &QFontComboBox::currentFontChanged,
            [this]() -> void { triggerSignal(); });
    /* connect comboFontSizes */
    connect(
        ui->comboFontSizes,
        static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
        [this]() -> void { triggerSignal(); });
    /*    */
    /* connect normal, bold, bolditalic */
    connect(ui->normal, &QAbstractButton::clicked, [this]() {
        triggerSignal();
    });
    connect(ui->bold, &QAbstractButton::clicked, [this]() {
        triggerSignal();
    });
    connect(ui->italic, &QAbstractButton::clicked, [this]() {
        triggerSignal();
    });
    connect(ui->boldItalic, &QAbstractButton::clicked, [this]() {
        //        emit triggerSignal();
        triggerSignal();
    });
    /* Exit */
    connect(ui->pbExit, &QAbstractButton::clicked, [this]() -> void {
        delete this;
    });
    /* Default */
    connect(ui->pbDefault, &QAbstractButton::clicked, [this]() -> void {
        int currentfontsize = this->font().pointSize();

        QString familj = DEFAULT_FAMILY;
        QFont defaultfont(familj, currentfontsize);
        ui->comboFilter->setCurrentIndex(0);
        ui->comboFontSizes->setCurrentText(QString::number(currentfontsize));
        ui->comboFontFamilies->setCurrentFont(defaultfont);
        ui->normal->setChecked(true);

        emit valueChanged(defaultfont);
    });
}

void SelectFont::triggerSignal()
{
    QString font = ui->comboFontFamilies->currentText();
    QString size = ui->comboFontSizes->currentText();
    QFont f = QFont(font, size.toInt());

    if(ui->boldItalic->isChecked()) {
        f.setBold(true);
        f.setItalic(true);
    } else {
        if(ui->bold->isChecked()) {
            f.setBold(true);
        } else if(ui->italic->isChecked()) {
            f.setItalic(true);
        } else if(ui->normal->isChecked()) {
            f.setBold(false);
            f.setItalic(false);
        }
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Font");
    settings.setValue("family", ui->comboFontFamilies->currentText());
    settings.setValue("size", ui->comboFontSizes->currentText());
    settings.setValue("boldItalic", ui->boldItalic->isChecked());
    settings.setValue("bold", ui->bold->isChecked());
    settings.setValue("italic", ui->italic->isChecked());
    settings.endGroup();
    emit valueChanged(f);
}

SelectFont::~SelectFont()
{
    triggerSignal();
    emit selectFontClose();
    delete ui;
}

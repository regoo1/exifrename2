//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef TABWIDGET_H
#define TABWIDGET_H
#include "info.h"
#include "mainwindow.h"
#include <QTabWidget>
#include <QDateTime>
#include <QSettings>
#include <QStringListModel>
#include <QInputDialog>
#include <QFileDevice>
// #include <QRegExp>
#include <QAbstractItemModel>

namespace Ui
{
class TabWidget;
}

class TabWidget : public QTabWidget
{

    Q_OBJECT

public:
    explicit TabWidget(QWidget *parent = nullptr);
    ~TabWidget();

private:
    Ui::TabWidget *ui;
    void setEndConfig();
    void setStartConfig();
    QChar indexToChar(int index);
    void readPlaceonscreen();
    void writePlaceonscreen();
    QFont *fmono;
    bool makeDesktopFile(QString path);
    bool chortcutExists(QString path);
    bool chortcutIsExecutable(QString path);
    void isUpdated(bool b);
    void firstRun();
    void testTranslation();

signals:
    void tabwidgetclosing();
#ifdef Q_OS_LINUX
    void mainwindowclosing();
#endif

private slots:
    void defaultExtensions();
    void newExtensions();
    void removeExtensions();
    void defaultExtensions2();
    void newExtensions2();
    void removeExtensions2();
    void selectCopyTo();
    void chortcutdesktop(int state);
    void chortcutapplications(int state);

public slots:
    static bool pathExist(QString path);

};

extern Q_CORE_EXPORT int qt_ntfs_permission_lookup;

#endif // TABWIDGET_H

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include "ui_mainwindow.h"

bool MainWindow::reName(QString &path, QString &name, bool dorename)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Tools");
    bool checkredtext = settings.value("checkredtext", false).toBool();
    bool displaysuccess = settings.value("displaysuccess", false).toBool();
    settings.endGroup();
#ifdef Q_OS_WIN
    qt_ntfs_permission_lookup++; // turn checking on
#endif
    QFileInfo fi(path);

    if(! fi.exists()) {
        return false;
    }

    QFile f(path);
    QFileDevice::Permissions filepermissions = f.permissions();

    if(!fi.isWritable()) {
        if(checkredtext) {
            ui->textBrowserMain->setTextColor(QColor("red"));
            ui->textBrowserMain->append(tr("The operating system does not allow you to change the file name") + " \"" + QDir::toNativeSeparators(path) + "\"");
            ui->textBrowserMain->setTextColor(QColor("black"));
        } else {
            ui->textBrowserMain->append(tr("The operating system does not allow you to change the file name") + " \"" + QDir::toNativeSeparators(path) + "\"");
        }

        return false;
    }

    if(! fi.exists()) {
        return false;
    }

    QString oldfile(fi.absoluteFilePath());

    if(! fi.permission(QFile::WriteUser)) {
        if(checkredtext) {
            ui->textBrowserMain->setTextColor(QColor("red"));
            ui->textBrowserMain->append(tr("The operating system does not allow you to change the file name") + " \"" + QDir::toNativeSeparators(oldfile) + "\"");
            ui->textBrowserMain->setTextColor(QColor("black"));
        } else {
            ui->textBrowserMain->append(tr("The operating system does not allow you to change the file name") + " \"" + QDir::toNativeSeparators(oldfile) + "\"");
        }

#ifdef Q_OS_WIN
        qt_ntfs_permission_lookup--; // turn checking off
#endif
        return false;
    }

    QString ext(fi.suffix());
    QString newfile(fi.canonicalPath() + "/" + name + "." + ext);
    QFileInfo fi2(newfile);
    QString filnamn  = fi2.fileName();
    newfile.remove(filnamn);
    settings.beginGroup("PathPattern");
    QChar datetimepattern = settings.value("datetimepattern", "_").toChar();
    QChar filenamepattern = settings.value("filenamepattern", "-").toChar();
    bool onlytimeinfilename = settings.value("onlytimeinfilename", false).toBool();
    QString currentinserttext = settings.value("currentinserttext", "").toString();
    settings.endGroup();
    settings.beginGroup("Tools");
    QString radiotext = settings.value("radiotext", "radionotext").toString();
    QString fileextension = settings.value("fileextension", "keep").toString();
    settings.endGroup();

    if(onlytimeinfilename) {
        filnamn[2] = filenamepattern;
        filnamn[5] = filenamepattern;
        newfile = newfile + filnamn;
    } else {
        filnamn[4] = filenamepattern;
        filnamn[7] = filenamepattern;
        filnamn[10] = datetimepattern;
        filnamn[13] = filenamepattern;
        filnamn[16] = filenamepattern;
        newfile = newfile + filnamn;
    }

    if((radiotext == "radioafter") && (!currentinserttext.isEmpty())) {
        QFileInfo fi(newfile);
        QString filnamn  = fi.fileName();
        QString ext(fi.suffix());
        newfile = newfile.remove(filnamn);
        filnamn = filnamn.remove("." + ext);
        newfile = newfile + filnamn + currentinserttext + "." + ext;
    } else if((radiotext == "radiobefore") && (!currentinserttext.isEmpty())) {
        QFileInfo fi(newfile);
        QString filnamn  = fi.fileName();
        QString ext(fi.suffix());
        newfile = newfile.remove(filnamn);
        filnamn = filnamn.remove("." + ext);
        newfile = newfile + currentinserttext + filnamn + "." + ext;
    }

    if(!dorename) {
        int i = 1;
        QFileInfo fi3(newfile);

        while(fi3.exists()) {
            newfile.remove("." + ext);
            newfile.remove("(" + QString::number(i - 1) + ")");
            newfile = newfile + "(" + QString::number(i) + ")." + ext;
            fi3.setFile(newfile);
            i++;
        }
    }

    fi.setFile(newfile);
    QString extension(fi.suffix());

    if(fileextension == "lowercase") {
        int pos = newfile.lastIndexOf(extension);
        newfile.remove(pos, extension.size());
        extension = extension.toLower();
        newfile = newfile + extension;
    } else if(fileextension == "capital") {
        int pos = newfile.lastIndexOf(extension);
        newfile.remove(pos, extension.size());
        extension = extension.toUpper();
        newfile = newfile + extension;
    }

    if(QFile::rename(oldfile, newfile)) {
#ifdef Q_OS_WIN
        qt_ntfs_permission_lookup--; // turn checking off
#endif
#ifdef Q_OS_LINUX
#endif
        QFile file(newfile);
        file.setPermissions(filepermissions);
        QString newfilename = fi.absoluteFilePath();

        if(displaysuccess || dorename) {
            ui->textBrowserMain->append(tr("Path and new name is") + " \"" + QDir::toNativeSeparators(newfilename) + "\"");
        }

        return true;
    }

    if(dorename) {
        QFileInfo fi(oldfile);
        QString oldfilename = fi.absoluteFilePath();
        ui->textBrowserMain->append("\"" + QDir::toNativeSeparators(oldfilename) + "\" " + tr("retains its original name"));
    }

    return false;
}

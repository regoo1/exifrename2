//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "tabwidget.h"
#include "ui_tabwidget.h"
#include "info.h"

bool TabWidget::pathExist(QString path)
{
    path = QDir::toNativeSeparators(path);
#ifdef Q_OS_WIN
    qt_ntfs_permission_lookup++; // turn checking on
#endif
    QFileInfo fi(path);

    if(fi.isWritable()) {
#ifdef Q_OS_WIN
        qt_ntfs_permission_lookup--; // turn it off again
#endif
        return true;
    }

#ifdef Q_OS_WIN
    qt_ntfs_permission_lookup--; // turn it off again
#endif
    return false;
}

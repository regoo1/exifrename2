//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef INFO_H
#define INFO_H

#include "mainwindow.h"
#include "tabwidget.h"
#include <QWidget>
#include <QtGlobal>
#include <sstream>
#include <string>

#define VERSION_PATH "http://bin.ceicer.com/exifrename2/version.txt"
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/exifrename2/-/wikis/DOWNLOADS"
#define MANUAL_PATH_ENG "https://bin.ceicer.com/exifrename2/manual/eng/"
#define MANUAL_PATH_SV "https://bin.ceicer.com/exifrename2/manual/eng/"
#define PROG_NAME "exifrename"
#define DISPLAY_NAME "EXIF ReName"
#define EXECUTABLE_NAME "exifrename"
#define VERSION "1.1.3"
#define LICENSE "https://bin.ceicer.com/exifrename2/LICENSE"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define START_YEAR "2007"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "ic_0002@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE                                                   \
    "http://ceicer.org/exifrename"
#define APPLICATION_HOMEPAGE_ENG                                               \
    "http://ceicer.org/exifrename"
#define SOURCEKODE "https://gitlab.com/posktomten/exifrename2"
#define WIKI "https://gitlab.com/posktomten/exifrename2/-/wikis/home"
#define RED 218
#define GREEN 255
#define BLUE 221

#include "exif.h"
#include "mainwindow.h"
#include "tabwidget.h"

#ifdef Q_OS_LINUX
#define FONTSIZE 11
#endif
#ifdef Q_OS_WIN
#define FONTSIZE 9
#endif

#ifdef Q_OS_LINUX
#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ARG1 "http://bin.ceicer.com/zsync/EXIF_ReName-i386.AppImage.zsync"
#define ARG2 "-i EXIF_ReName-i386.AppImage"
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define ARG1 "http://bin.ceicer.com/zsync/EXIF_ReName-x86_64.AppImage.zsync"
#define ARG2 "-i EXIF_ReName-x86_64.AppImage"
#endif
#endif

#ifdef Q_OS_WIN
#define portable
#endif
class Info : public QWidget
{
    Q_OBJECT
public:
    QString getSystem();
};

#endif // INFO_H

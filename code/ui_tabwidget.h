/********************************************************************************
** Form generated from reading UI file 'tabwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.12
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABWIDGET_H
#define UI_TABWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabWidget
{
public:
    QWidget *tabFileName;
    QLabel *lblCurrentDateTime;
    QFrame *frame;
    QComboBox *cmbFileName;
    QLabel *lblFileName;
    QFrame *frame_2;
    QLabel *lblFolderName;
    QComboBox *cmbFolderName;
    QFrame *frame_3;
    QLabel *lblDateTime;
    QComboBox *cmbDateTime;
    QLabel *lblDisplayFilename;
    QLabel *lblDisplayFoldername;
    QLabel *lblFolder;
    QFrame *frame_4;
    QLabel *lblExtension;
    QRadioButton *radioCapital;
    QRadioButton *radioLowercase;
    QRadioButton *radioKeep;
    QFrame *frame_5;
    QCheckBox *checkOnlyTime;
    QCheckBox *checkExtension;
    QCheckBox *checkYears;
    QFrame *frame_6;
    QComboBox *cmbInsert;
    QLabel *lblInsert;
    QRadioButton *radioBefore;
    QRadioButton *radioAfter;
    QLabel *lblSaveDelete;
    QPushButton *pbSave;
    QPushButton *pbRemove;
    QRadioButton *radioNoText;
    QPushButton *pbClose;
    QWidget *tabExtensions;
    QFrame *frame_7;
    QComboBox *cmbExtensions;
    QLabel *lblMyFileExtensions;
    QPushButton *pbNewExtension;
    QPushButton *pbRemoveExtension;
    QPushButton *pbDefaultExtension;
    QLabel *lblMyFileExtensionsPattern;
    QLabel *lblMyFileExtension2;
    QPushButton *pbCloseExtension;
    QFrame *frame_16;
    QComboBox *cmbExtensions_2;
    QLabel *lblMyFileExtensions_2;
    QPushButton *pbNewExtension_2;
    QPushButton *pbRemoveExtension_2;
    QPushButton *pbDefaultExtension_2;
    QLabel *lblMyFileExtension2_2;
    QWidget *tabOpenCopy;
    QFrame *frame_9;
    QLabel *lblSetLocation;
    QPushButton *pbSetLocation;
    QLabel *lblCopyLocation;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *radioDontCopyToDefault;
    QRadioButton *radioCopyToDefault;
    QPushButton *pbCloseOpenCopy;
    QFrame *frame_10;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioLastUsed;
    QRadioButton *radioHome;
    QFrame *frame_12;
    QWidget *layoutWidget2_2;
    QVBoxLayout *verticalLayout_3;
    QRadioButton *radioDragDroppRename;
    QRadioButton *radioDragDroppCopy;
    QWidget *tabMiscellaneous;
    QFrame *frame_11;
    QCheckBox *checkDoCheckOnStart;
    QFrame *frame_8;
    QCheckBox *checkNativeDialog;
    QFrame *frame_13;
    QPushButton *pbCloseMiscellaneous;
    QFrame *frame_15;
    QCheckBox *checkChortcutApplicationsLocation;
    QCheckBox *checkChortcutDesktopLocation;
    QCheckBox *checkToolTips;
    QFrame *frame_14;
    QToolButton *pbForceUpdate;
    QFrame *frame_17;
    QCheckBox *checkDisplayFailure;
    QCheckBox *checkDisplaySuccess;
    QCheckBox *checkRedText;
    QFrame *frame_19;
    QCheckBox *checkActionRename;
    QLabel *lblToolbar;
    QCheckBox *checkActionClose;
    QCheckBox *checkActionRenameRecursively;
    QCheckBox *checkActionCopyRename;
    QCheckBox *checkActionClear;
    QCheckBox *checkActionEditExif;
    QCheckBox *checkActionEditExifRename;
    QCheckBox *checkActionEditExifOffset;
    QCheckBox *checkActionEditExifOffsetRename;
    QFrame *frame_18;
    QPushButton *pbTestLanguagefile;
    QWidget *tabLicense;
    QGridLayout *gridLayout_2;
    QTextBrowser *textBrowserLicense;
    QPushButton *pbCloseLicense;
    QWidget *tabAbout;
    QGridLayout *gridLayout;
    QTextBrowser *textBrowser;
    QPushButton *pbCloseAbout;

    void setupUi(QTabWidget *TabWidget)
    {
        if (TabWidget->objectName().isEmpty())
            TabWidget->setObjectName(QString::fromUtf8("TabWidget"));
        TabWidget->setWindowModality(Qt::ApplicationModal);
        TabWidget->resize(725, 637);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(113);
        sizePolicy.setHeightForWidth(TabWidget->sizePolicy().hasHeightForWidth());
        TabWidget->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/exifrename.png"), QSize(), QIcon::Normal, QIcon::Off);
        TabWidget->setWindowIcon(icon);
        TabWidget->setAutoFillBackground(true);
        TabWidget->setTabShape(QTabWidget::Rounded);
        TabWidget->setElideMode(Qt::ElideNone);
        TabWidget->setDocumentMode(false);
        tabFileName = new QWidget();
        tabFileName->setObjectName(QString::fromUtf8("tabFileName"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(24);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabFileName->sizePolicy().hasHeightForWidth());
        tabFileName->setSizePolicy(sizePolicy1);
        tabFileName->setMaximumSize(QSize(16777205, 16777215));
        lblCurrentDateTime = new QLabel(tabFileName);
        lblCurrentDateTime->setObjectName(QString::fromUtf8("lblCurrentDateTime"));
        lblCurrentDateTime->setGeometry(QRect(160, 40, 481, 25));
        frame = new QFrame(tabFileName);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(480, 80, 233, 131));
        frame->setFrameShape(QFrame::WinPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame->setMidLineWidth(0);
        cmbFileName = new QComboBox(frame);
        cmbFileName->addItem(QString());
        cmbFileName->addItem(QString());
        cmbFileName->addItem(QString());
        cmbFileName->setObjectName(QString::fromUtf8("cmbFileName"));
        cmbFileName->setGeometry(QRect(10, 80, 190, 35));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(cmbFileName->sizePolicy().hasHeightForWidth());
        cmbFileName->setSizePolicy(sizePolicy2);
        cmbFileName->setMinimumSize(QSize(190, 35));
        cmbFileName->setMaximumSize(QSize(190, 35));
        cmbFileName->setMaxVisibleItems(3);
        lblFileName = new QLabel(frame);
        lblFileName->setObjectName(QString::fromUtf8("lblFileName"));
        lblFileName->setGeometry(QRect(20, 10, 171, 75));
        sizePolicy2.setHeightForWidth(lblFileName->sizePolicy().hasHeightForWidth());
        lblFileName->setSizePolicy(sizePolicy2);
        lblFileName->setFrameShape(QFrame::NoFrame);
        lblFileName->setWordWrap(true);
        frame_2 = new QFrame(tabFileName);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(10, 80, 233, 131));
        frame_2->setFrameShape(QFrame::WinPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        lblFolderName = new QLabel(frame_2);
        lblFolderName->setObjectName(QString::fromUtf8("lblFolderName"));
        lblFolderName->setGeometry(QRect(10, 10, 191, 75));
        sizePolicy2.setHeightForWidth(lblFolderName->sizePolicy().hasHeightForWidth());
        lblFolderName->setSizePolicy(sizePolicy2);
        lblFolderName->setWordWrap(true);
        cmbFolderName = new QComboBox(frame_2);
        cmbFolderName->addItem(QString());
        cmbFolderName->addItem(QString());
        cmbFolderName->addItem(QString());
        cmbFolderName->setObjectName(QString::fromUtf8("cmbFolderName"));
        cmbFolderName->setGeometry(QRect(10, 80, 190, 35));
        sizePolicy2.setHeightForWidth(cmbFolderName->sizePolicy().hasHeightForWidth());
        cmbFolderName->setSizePolicy(sizePolicy2);
        cmbFolderName->setMinimumSize(QSize(190, 35));
        cmbFolderName->setMaximumSize(QSize(190, 35));
        cmbFolderName->setMaxVisibleItems(3);
        cmbFolderName->setInsertPolicy(QComboBox::InsertAtTop);
        cmbFolderName->setFrame(true);
        cmbFolderName->raise();
        lblFolderName->raise();
        frame_3 = new QFrame(tabFileName);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(245, 80, 233, 131));
        frame_3->setFrameShape(QFrame::WinPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        frame_3->setLineWidth(1);
        frame_3->setMidLineWidth(0);
        lblDateTime = new QLabel(frame_3);
        lblDateTime->setObjectName(QString::fromUtf8("lblDateTime"));
        lblDateTime->setGeometry(QRect(10, 10, 191, 75));
        sizePolicy2.setHeightForWidth(lblDateTime->sizePolicy().hasHeightForWidth());
        lblDateTime->setSizePolicy(sizePolicy2);
        lblDateTime->setWordWrap(true);
        cmbDateTime = new QComboBox(frame_3);
        cmbDateTime->addItem(QString());
        cmbDateTime->addItem(QString());
        cmbDateTime->addItem(QString());
        cmbDateTime->setObjectName(QString::fromUtf8("cmbDateTime"));
        cmbDateTime->setGeometry(QRect(10, 80, 190, 35));
        sizePolicy2.setHeightForWidth(cmbDateTime->sizePolicy().hasHeightForWidth());
        cmbDateTime->setSizePolicy(sizePolicy2);
        cmbDateTime->setMinimumSize(QSize(190, 35));
        cmbDateTime->setMaximumSize(QSize(190, 35));
        cmbDateTime->setMaxVisibleItems(3);
        lblDisplayFilename = new QLabel(tabFileName);
        lblDisplayFilename->setObjectName(QString::fromUtf8("lblDisplayFilename"));
        lblDisplayFilename->setGeometry(QRect(20, 40, 100, 25));
        lblDisplayFoldername = new QLabel(tabFileName);
        lblDisplayFoldername->setObjectName(QString::fromUtf8("lblDisplayFoldername"));
        lblDisplayFoldername->setGeometry(QRect(20, 10, 111, 25));
        lblFolder = new QLabel(tabFileName);
        lblFolder->setObjectName(QString::fromUtf8("lblFolder"));
        lblFolder->setGeometry(QRect(160, 10, 421, 25));
        frame_4 = new QFrame(tabFileName);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setGeometry(QRect(10, 215, 233, 141));
        frame_4->setFrameShape(QFrame::WinPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        lblExtension = new QLabel(frame_4);
        lblExtension->setObjectName(QString::fromUtf8("lblExtension"));
        lblExtension->setGeometry(QRect(10, 0, 171, 31));
        lblExtension->setWordWrap(true);
        radioCapital = new QRadioButton(frame_4);
        radioCapital->setObjectName(QString::fromUtf8("radioCapital"));
        radioCapital->setGeometry(QRect(10, 105, 146, 23));
        radioLowercase = new QRadioButton(frame_4);
        radioLowercase->setObjectName(QString::fromUtf8("radioLowercase"));
        radioLowercase->setGeometry(QRect(10, 80, 171, 23));
        radioKeep = new QRadioButton(frame_4);
        radioKeep->setObjectName(QString::fromUtf8("radioKeep"));
        radioKeep->setGeometry(QRect(10, 35, 211, 41));
        radioKeep->setChecked(true);
        frame_5 = new QFrame(tabFileName);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setGeometry(QRect(245, 215, 231, 141));
        frame_5->setFrameShape(QFrame::WinPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        checkOnlyTime = new QCheckBox(frame_5);
        checkOnlyTime->setObjectName(QString::fromUtf8("checkOnlyTime"));
        checkOnlyTime->setGeometry(QRect(5, 104, 206, 23));
        checkExtension = new QCheckBox(frame_5);
        checkExtension->setObjectName(QString::fromUtf8("checkExtension"));
        checkExtension->setGeometry(QRect(5, 3, 221, 41));
        checkExtension->setChecked(false);
        checkYears = new QCheckBox(frame_5);
        checkYears->setObjectName(QString::fromUtf8("checkYears"));
        checkYears->setGeometry(QRect(5, 49, 221, 41));
        checkYears->setChecked(false);
        frame_6 = new QFrame(tabFileName);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setGeometry(QRect(10, 360, 701, 151));
        frame_6->setFrameShape(QFrame::WinPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        cmbInsert = new QComboBox(frame_6);
        cmbInsert->setObjectName(QString::fromUtf8("cmbInsert"));
        cmbInsert->setGeometry(QRect(10, 110, 681, 30));
        cmbInsert->setEditable(true);
        lblInsert = new QLabel(frame_6);
        lblInsert->setObjectName(QString::fromUtf8("lblInsert"));
        lblInsert->setGeometry(QRect(15, 45, 211, 25));
        radioBefore = new QRadioButton(frame_6);
        radioBefore->setObjectName(QString::fromUtf8("radioBefore"));
        radioBefore->setGeometry(QRect(235, 45, 201, 25));
        radioAfter = new QRadioButton(frame_6);
        radioAfter->setObjectName(QString::fromUtf8("radioAfter"));
        radioAfter->setGeometry(QRect(235, 75, 201, 25));
        radioAfter->setMinimumSize(QSize(8, 0));
        lblSaveDelete = new QLabel(frame_6);
        lblSaveDelete->setObjectName(QString::fromUtf8("lblSaveDelete"));
        lblSaveDelete->setGeometry(QRect(490, 10, 201, 50));
        lblSaveDelete->setWordWrap(true);
        pbSave = new QPushButton(frame_6);
        pbSave->setObjectName(QString::fromUtf8("pbSave"));
        pbSave->setGeometry(QRect(490, 60, 89, 35));
        pbRemove = new QPushButton(frame_6);
        pbRemove->setObjectName(QString::fromUtf8("pbRemove"));
        pbRemove->setGeometry(QRect(590, 60, 89, 35));
        radioNoText = new QRadioButton(frame_6);
        radioNoText->setObjectName(QString::fromUtf8("radioNoText"));
        radioNoText->setGeometry(QRect(235, 15, 201, 25));
        radioNoText->setChecked(true);
        pbClose = new QPushButton(tabFileName);
        pbClose->setObjectName(QString::fromUtf8("pbClose"));
        pbClose->setGeometry(QRect(11, 550, 93, 35));
        TabWidget->addTab(tabFileName, QString());
        frame_3->raise();
        frame_2->raise();
        frame->raise();
        lblCurrentDateTime->raise();
        lblDisplayFilename->raise();
        lblDisplayFoldername->raise();
        lblFolder->raise();
        frame_4->raise();
        frame_5->raise();
        frame_6->raise();
        pbClose->raise();
        tabExtensions = new QWidget();
        tabExtensions->setObjectName(QString::fromUtf8("tabExtensions"));
        frame_7 = new QFrame(tabExtensions);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setGeometry(QRect(10, 10, 701, 251));
        frame_7->setFrameShape(QFrame::WinPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        cmbExtensions = new QComboBox(frame_7);
        cmbExtensions->setObjectName(QString::fromUtf8("cmbExtensions"));
        cmbExtensions->setGeometry(QRect(20, 200, 661, 30));
        cmbExtensions->setEditable(false);
        cmbExtensions->setInsertPolicy(QComboBox::NoInsert);
        cmbExtensions->setSizeAdjustPolicy(QComboBox::AdjustToContentsOnFirstShow);
        lblMyFileExtensions = new QLabel(frame_7);
        lblMyFileExtensions->setObjectName(QString::fromUtf8("lblMyFileExtensions"));
        lblMyFileExtensions->setGeometry(QRect(20, 50, 501, 31));
        pbNewExtension = new QPushButton(frame_7);
        pbNewExtension->setObjectName(QString::fromUtf8("pbNewExtension"));
        pbNewExtension->setGeometry(QRect(150, 150, 93, 35));
        pbRemoveExtension = new QPushButton(frame_7);
        pbRemoveExtension->setObjectName(QString::fromUtf8("pbRemoveExtension"));
        pbRemoveExtension->setGeometry(QRect(280, 150, 93, 35));
        pbDefaultExtension = new QPushButton(frame_7);
        pbDefaultExtension->setObjectName(QString::fromUtf8("pbDefaultExtension"));
        pbDefaultExtension->setGeometry(QRect(20, 150, 93, 35));
        lblMyFileExtensionsPattern = new QLabel(frame_7);
        lblMyFileExtensionsPattern->setObjectName(QString::fromUtf8("lblMyFileExtensionsPattern"));
        lblMyFileExtensionsPattern->setGeometry(QRect(20, 80, 661, 31));
        lblMyFileExtension2 = new QLabel(frame_7);
        lblMyFileExtension2->setObjectName(QString::fromUtf8("lblMyFileExtension2"));
        lblMyFileExtension2->setGeometry(QRect(20, 20, 501, 31));
        pbCloseExtension = new QPushButton(tabExtensions);
        pbCloseExtension->setObjectName(QString::fromUtf8("pbCloseExtension"));
        pbCloseExtension->setGeometry(QRect(11, 550, 93, 35));
        frame_16 = new QFrame(tabExtensions);
        frame_16->setObjectName(QString::fromUtf8("frame_16"));
        frame_16->setGeometry(QRect(10, 268, 701, 251));
        frame_16->setFrameShape(QFrame::WinPanel);
        frame_16->setFrameShadow(QFrame::Raised);
        cmbExtensions_2 = new QComboBox(frame_16);
        cmbExtensions_2->setObjectName(QString::fromUtf8("cmbExtensions_2"));
        cmbExtensions_2->setGeometry(QRect(20, 200, 91, 30));
        cmbExtensions_2->setEditable(false);
        cmbExtensions_2->setInsertPolicy(QComboBox::NoInsert);
        cmbExtensions_2->setSizeAdjustPolicy(QComboBox::AdjustToContentsOnFirstShow);
        lblMyFileExtensions_2 = new QLabel(frame_16);
        lblMyFileExtensions_2->setObjectName(QString::fromUtf8("lblMyFileExtensions_2"));
        lblMyFileExtensions_2->setGeometry(QRect(20, 50, 501, 31));
        pbNewExtension_2 = new QPushButton(frame_16);
        pbNewExtension_2->setObjectName(QString::fromUtf8("pbNewExtension_2"));
        pbNewExtension_2->setGeometry(QRect(150, 150, 93, 35));
        pbRemoveExtension_2 = new QPushButton(frame_16);
        pbRemoveExtension_2->setObjectName(QString::fromUtf8("pbRemoveExtension_2"));
        pbRemoveExtension_2->setGeometry(QRect(280, 150, 93, 35));
        pbDefaultExtension_2 = new QPushButton(frame_16);
        pbDefaultExtension_2->setObjectName(QString::fromUtf8("pbDefaultExtension_2"));
        pbDefaultExtension_2->setGeometry(QRect(20, 150, 93, 35));
        lblMyFileExtension2_2 = new QLabel(frame_16);
        lblMyFileExtension2_2->setObjectName(QString::fromUtf8("lblMyFileExtension2_2"));
        lblMyFileExtension2_2->setGeometry(QRect(20, 20, 666, 31));
        TabWidget->addTab(tabExtensions, QString());
        tabOpenCopy = new QWidget();
        tabOpenCopy->setObjectName(QString::fromUtf8("tabOpenCopy"));
        frame_9 = new QFrame(tabOpenCopy);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        frame_9->setGeometry(QRect(10, 5, 701, 248));
        frame_9->setFrameShape(QFrame::WinPanel);
        frame_9->setFrameShadow(QFrame::Raised);
        lblSetLocation = new QLabel(frame_9);
        lblSetLocation->setObjectName(QString::fromUtf8("lblSetLocation"));
        lblSetLocation->setGeometry(QRect(20, 10, 411, 35));
        pbSetLocation = new QPushButton(frame_9);
        pbSetLocation->setObjectName(QString::fromUtf8("pbSetLocation"));
        pbSetLocation->setGeometry(QRect(20, 50, 93, 35));
        pbSetLocation->setMinimumSize(QSize(93, 35));
        pbSetLocation->setMaximumSize(QSize(93, 35));
        lblCopyLocation = new QLabel(frame_9);
        lblCopyLocation->setObjectName(QString::fromUtf8("lblCopyLocation"));
        lblCopyLocation->setGeometry(QRect(20, 100, 661, 70));
        lblCopyLocation->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lblCopyLocation->setWordWrap(true);
        layoutWidget = new QWidget(frame_9);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 180, 666, 54));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        radioDontCopyToDefault = new QRadioButton(layoutWidget);
        radioDontCopyToDefault->setObjectName(QString::fromUtf8("radioDontCopyToDefault"));
        radioDontCopyToDefault->setMaximumSize(QSize(500, 16777215));
        radioDontCopyToDefault->setChecked(true);

        verticalLayout_2->addWidget(radioDontCopyToDefault);

        radioCopyToDefault = new QRadioButton(layoutWidget);
        radioCopyToDefault->setObjectName(QString::fromUtf8("radioCopyToDefault"));
        radioCopyToDefault->setMaximumSize(QSize(500, 16777215));

        verticalLayout_2->addWidget(radioCopyToDefault);

        pbCloseOpenCopy = new QPushButton(tabOpenCopy);
        pbCloseOpenCopy->setObjectName(QString::fromUtf8("pbCloseOpenCopy"));
        pbCloseOpenCopy->setGeometry(QRect(11, 550, 93, 35));
        pbCloseOpenCopy->setMinimumSize(QSize(93, 35));
        pbCloseOpenCopy->setMaximumSize(QSize(93, 35));
        frame_10 = new QFrame(tabOpenCopy);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        frame_10->setGeometry(QRect(10, 260, 701, 76));
        frame_10->setFrameShape(QFrame::WinPanel);
        frame_10->setFrameShadow(QFrame::Raised);
        layoutWidget2 = new QWidget(frame_10);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 10, 661, 54));
        verticalLayout = new QVBoxLayout(layoutWidget2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        radioLastUsed = new QRadioButton(layoutWidget2);
        radioLastUsed->setObjectName(QString::fromUtf8("radioLastUsed"));
        radioLastUsed->setMaximumSize(QSize(500, 16777215));
        radioLastUsed->setChecked(true);

        verticalLayout->addWidget(radioLastUsed);

        radioHome = new QRadioButton(layoutWidget2);
        radioHome->setObjectName(QString::fromUtf8("radioHome"));
        radioHome->setMaximumSize(QSize(500, 16777215));

        verticalLayout->addWidget(radioHome);

        frame_12 = new QFrame(tabOpenCopy);
        frame_12->setObjectName(QString::fromUtf8("frame_12"));
        frame_12->setGeometry(QRect(10, 340, 701, 76));
        frame_12->setFrameShape(QFrame::WinPanel);
        frame_12->setFrameShadow(QFrame::Raised);
        layoutWidget2_2 = new QWidget(frame_12);
        layoutWidget2_2->setObjectName(QString::fromUtf8("layoutWidget2_2"));
        layoutWidget2_2->setGeometry(QRect(10, 10, 666, 54));
        verticalLayout_3 = new QVBoxLayout(layoutWidget2_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        radioDragDroppRename = new QRadioButton(layoutWidget2_2);
        radioDragDroppRename->setObjectName(QString::fromUtf8("radioDragDroppRename"));
        radioDragDroppRename->setMaximumSize(QSize(500, 16777215));
        radioDragDroppRename->setChecked(true);

        verticalLayout_3->addWidget(radioDragDroppRename);

        radioDragDroppCopy = new QRadioButton(layoutWidget2_2);
        radioDragDroppCopy->setObjectName(QString::fromUtf8("radioDragDroppCopy"));
        radioDragDroppCopy->setMaximumSize(QSize(500, 16777215));

        verticalLayout_3->addWidget(radioDragDroppCopy);

        TabWidget->addTab(tabOpenCopy, QString());
        tabMiscellaneous = new QWidget();
        tabMiscellaneous->setObjectName(QString::fromUtf8("tabMiscellaneous"));
        frame_11 = new QFrame(tabMiscellaneous);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        frame_11->setGeometry(QRect(10, 10, 701, 51));
        frame_11->setFrameShape(QFrame::WinPanel);
        frame_11->setFrameShadow(QFrame::Raised);
        checkDoCheckOnStart = new QCheckBox(frame_11);
        checkDoCheckOnStart->setObjectName(QString::fromUtf8("checkDoCheckOnStart"));
        checkDoCheckOnStart->setGeometry(QRect(10, 10, 521, 28));
        checkDoCheckOnStart->setChecked(true);
        frame_8 = new QFrame(tabMiscellaneous);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setGeometry(QRect(10, 70, 701, 51));
        frame_8->setFrameShape(QFrame::WinPanel);
        frame_8->setFrameShadow(QFrame::Raised);
        checkNativeDialog = new QCheckBox(frame_8);
        checkNativeDialog->setObjectName(QString::fromUtf8("checkNativeDialog"));
        checkNativeDialog->setGeometry(QRect(10, 10, 611, 28));
        checkNativeDialog->setChecked(true);
        frame_13 = new QFrame(frame_8);
        frame_13->setObjectName(QString::fromUtf8("frame_13"));
        frame_13->setGeometry(QRect(0, 0, 701, 51));
        frame_13->setFrameShape(QFrame::WinPanel);
        frame_13->setFrameShadow(QFrame::Raised);
        frame_13->raise();
        checkNativeDialog->raise();
        pbCloseMiscellaneous = new QPushButton(tabMiscellaneous);
        pbCloseMiscellaneous->setObjectName(QString::fromUtf8("pbCloseMiscellaneous"));
        pbCloseMiscellaneous->setGeometry(QRect(11, 550, 93, 35));
        pbCloseMiscellaneous->setMinimumSize(QSize(93, 35));
        pbCloseMiscellaneous->setMaximumSize(QSize(93, 35));
        frame_15 = new QFrame(tabMiscellaneous);
        frame_15->setObjectName(QString::fromUtf8("frame_15"));
        frame_15->setGeometry(QRect(10, 190, 701, 86));
        frame_15->setFrameShape(QFrame::WinPanel);
        frame_15->setFrameShadow(QFrame::Raised);
        checkChortcutApplicationsLocation = new QCheckBox(frame_15);
        checkChortcutApplicationsLocation->setObjectName(QString::fromUtf8("checkChortcutApplicationsLocation"));
        checkChortcutApplicationsLocation->setGeometry(QRect(10, 44, 371, 28));
        checkChortcutDesktopLocation = new QCheckBox(frame_15);
        checkChortcutDesktopLocation->setObjectName(QString::fromUtf8("checkChortcutDesktopLocation"));
        checkChortcutDesktopLocation->setGeometry(QRect(12, 12, 356, 28));
        checkToolTips = new QCheckBox(frame_15);
        checkToolTips->setObjectName(QString::fromUtf8("checkToolTips"));
        checkToolTips->setGeometry(QRect(415, 12, 256, 20));
        frame_14 = new QFrame(tabMiscellaneous);
        frame_14->setObjectName(QString::fromUtf8("frame_14"));
        frame_14->setEnabled(true);
        frame_14->setGeometry(QRect(10, 130, 701, 51));
        frame_14->setFrameShape(QFrame::WinPanel);
        frame_14->setFrameShadow(QFrame::Raised);
        pbForceUpdate = new QToolButton(frame_14);
        pbForceUpdate->setObjectName(QString::fromUtf8("pbForceUpdate"));
        pbForceUpdate->setGeometry(QRect(10, 8, 161, 35));
        frame_17 = new QFrame(tabMiscellaneous);
        frame_17->setObjectName(QString::fromUtf8("frame_17"));
        frame_17->setGeometry(QRect(10, 283, 471, 86));
        frame_17->setFrameShape(QFrame::WinPanel);
        frame_17->setFrameShadow(QFrame::Raised);
        checkDisplayFailure = new QCheckBox(frame_17);
        checkDisplayFailure->setObjectName(QString::fromUtf8("checkDisplayFailure"));
        checkDisplayFailure->setGeometry(QRect(10, 44, 221, 28));
        checkDisplayFailure->setChecked(true);
        checkDisplaySuccess = new QCheckBox(frame_17);
        checkDisplaySuccess->setObjectName(QString::fromUtf8("checkDisplaySuccess"));
        checkDisplaySuccess->setGeometry(QRect(12, 12, 166, 28));
        checkRedText = new QCheckBox(frame_17);
        checkRedText->setObjectName(QString::fromUtf8("checkRedText"));
        checkRedText->setGeometry(QRect(190, 12, 271, 28));
        checkRedText->setAutoFillBackground(false);
        checkRedText->setChecked(true);
        frame_19 = new QFrame(tabMiscellaneous);
        frame_19->setObjectName(QString::fromUtf8("frame_19"));
        frame_19->setGeometry(QRect(10, 376, 701, 149));
        frame_19->setFrameShape(QFrame::WinPanel);
        frame_19->setFrameShadow(QFrame::Raised);
        checkActionRename = new QCheckBox(frame_19);
        checkActionRename->setObjectName(QString::fromUtf8("checkActionRename"));
        checkActionRename->setGeometry(QRect(10, 55, 121, 23));
        lblToolbar = new QLabel(frame_19);
        lblToolbar->setObjectName(QString::fromUtf8("lblToolbar"));
        lblToolbar->setGeometry(QRect(10, 9, 651, 17));
        checkActionClose = new QCheckBox(frame_19);
        checkActionClose->setObjectName(QString::fromUtf8("checkActionClose"));
        checkActionClose->setGeometry(QRect(10, 35, 121, 23));
        checkActionRenameRecursively = new QCheckBox(frame_19);
        checkActionRenameRecursively->setObjectName(QString::fromUtf8("checkActionRenameRecursively"));
        checkActionRenameRecursively->setGeometry(QRect(10, 75, 211, 23));
        checkActionCopyRename = new QCheckBox(frame_19);
        checkActionCopyRename->setObjectName(QString::fromUtf8("checkActionCopyRename"));
        checkActionCopyRename->setGeometry(QRect(10, 95, 196, 23));
        checkActionClear = new QCheckBox(frame_19);
        checkActionClear->setObjectName(QString::fromUtf8("checkActionClear"));
        checkActionClear->setGeometry(QRect(10, 118, 121, 23));
        checkActionEditExif = new QCheckBox(frame_19);
        checkActionEditExif->setObjectName(QString::fromUtf8("checkActionEditExif"));
        checkActionEditExif->setGeometry(QRect(230, 35, 221, 23));
        checkActionEditExifRename = new QCheckBox(frame_19);
        checkActionEditExifRename->setObjectName(QString::fromUtf8("checkActionEditExifRename"));
        checkActionEditExifRename->setGeometry(QRect(230, 55, 411, 23));
        checkActionEditExifOffset = new QCheckBox(frame_19);
        checkActionEditExifOffset->setObjectName(QString::fromUtf8("checkActionEditExifOffset"));
        checkActionEditExifOffset->setGeometry(QRect(230, 75, 401, 23));
        checkActionEditExifOffsetRename = new QCheckBox(frame_19);
        checkActionEditExifOffsetRename->setObjectName(QString::fromUtf8("checkActionEditExifOffsetRename"));
        checkActionEditExifOffsetRename->setGeometry(QRect(230, 95, 451, 23));
        frame_18 = new QFrame(tabMiscellaneous);
        frame_18->setObjectName(QString::fromUtf8("frame_18"));
        frame_18->setGeometry(QRect(484, 283, 226, 86));
        frame_18->setFrameShape(QFrame::WinPanel);
        frame_18->setFrameShadow(QFrame::Raised);
        pbTestLanguagefile = new QPushButton(frame_18);
        pbTestLanguagefile->setObjectName(QString::fromUtf8("pbTestLanguagefile"));
        pbTestLanguagefile->setGeometry(QRect(10, 24, 201, 35));
        TabWidget->addTab(tabMiscellaneous, QString());
        tabLicense = new QWidget();
        tabLicense->setObjectName(QString::fromUtf8("tabLicense"));
        gridLayout_2 = new QGridLayout(tabLicense);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        textBrowserLicense = new QTextBrowser(tabLicense);
        textBrowserLicense->setObjectName(QString::fromUtf8("textBrowserLicense"));

        gridLayout_2->addWidget(textBrowserLicense, 0, 0, 1, 1);

        pbCloseLicense = new QPushButton(tabLicense);
        pbCloseLicense->setObjectName(QString::fromUtf8("pbCloseLicense"));
        pbCloseLicense->setMinimumSize(QSize(93, 35));
        pbCloseLicense->setMaximumSize(QSize(93, 35));

        gridLayout_2->addWidget(pbCloseLicense, 1, 0, 1, 1);

        TabWidget->addTab(tabLicense, QString());
        tabAbout = new QWidget();
        tabAbout->setObjectName(QString::fromUtf8("tabAbout"));
        gridLayout = new QGridLayout(tabAbout);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        textBrowser = new QTextBrowser(tabAbout);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));

        gridLayout->addWidget(textBrowser, 0, 0, 1, 1);

        pbCloseAbout = new QPushButton(tabAbout);
        pbCloseAbout->setObjectName(QString::fromUtf8("pbCloseAbout"));
        pbCloseAbout->setMinimumSize(QSize(93, 35));
        pbCloseAbout->setMaximumSize(QSize(93, 35));

        gridLayout->addWidget(pbCloseAbout, 1, 0, 1, 1);

        TabWidget->addTab(tabAbout, QString());

        retranslateUi(TabWidget);

        TabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(TabWidget);
    } // setupUi

    void retranslateUi(QTabWidget *TabWidget)
    {
        TabWidget->setWindowTitle(QCoreApplication::translate("TabWidget", "TabWidget", nullptr));
        lblCurrentDateTime->setText(QCoreApplication::translate("TabWidget", "lblCurrentDateTime", nullptr));
        cmbFileName->setItemText(0, QCoreApplication::translate("TabWidget", "Space", nullptr));
        cmbFileName->setItemText(1, QCoreApplication::translate("TabWidget", "Hyphen -", nullptr));
        cmbFileName->setItemText(2, QCoreApplication::translate("TabWidget", "Underscore _", nullptr));

        lblFileName->setText(QCoreApplication::translate("TabWidget", "File name figures,<br>separeted by", nullptr));
        lblFolderName->setText(QCoreApplication::translate("TabWidget", "Folder name figures,<br>separeted by", nullptr));
        cmbFolderName->setItemText(0, QCoreApplication::translate("TabWidget", "Space", nullptr));
        cmbFolderName->setItemText(1, QCoreApplication::translate("TabWidget", "Hyphen -", nullptr));
        cmbFolderName->setItemText(2, QCoreApplication::translate("TabWidget", "Underscore _", nullptr));

        lblDateTime->setText(QCoreApplication::translate("TabWidget", "Date and Time,<br>separeted by", nullptr));
        cmbDateTime->setItemText(0, QCoreApplication::translate("TabWidget", "Space", nullptr));
        cmbDateTime->setItemText(1, QCoreApplication::translate("TabWidget", "Hyphen -", nullptr));
        cmbDateTime->setItemText(2, QCoreApplication::translate("TabWidget", "Underscore _", nullptr));

        lblDisplayFilename->setText(QCoreApplication::translate("TabWidget", "File name:", nullptr));
        lblDisplayFoldername->setText(QCoreApplication::translate("TabWidget", "Folder name:", nullptr));
        lblFolder->setText(QCoreApplication::translate("TabWidget", "lblFolder", nullptr));
        lblExtension->setText(QCoreApplication::translate("TabWidget", "Extension", nullptr));
        radioCapital->setText(QCoreApplication::translate("TabWidget", "CAPITAL", nullptr));
        radioLowercase->setText(QCoreApplication::translate("TabWidget", "lowercase", nullptr));
        radioKeep->setText(QCoreApplication::translate("TabWidget", "Keep the original\n"
"lowercase or UPPERCASE", nullptr));
        checkOnlyTime->setText(QCoreApplication::translate("TabWidget", "Only time in file name", nullptr));
        checkExtension->setText(QCoreApplication::translate("TabWidget", "Folder named after\n"
"the file name extension", nullptr));
        checkYears->setText(QCoreApplication::translate("TabWidget", "Folder named after the year\n"
"the picture was taken", nullptr));
        lblInsert->setText(QCoreApplication::translate("TabWidget", "Add text to the file name", nullptr));
        radioBefore->setText(QCoreApplication::translate("TabWidget", "In the beginning", nullptr));
        radioAfter->setText(QCoreApplication::translate("TabWidget", "In the end", nullptr));
        lblSaveDelete->setText(QCoreApplication::translate("TabWidget", "Save or remove text", nullptr));
        pbSave->setText(QCoreApplication::translate("TabWidget", "Save", nullptr));
        pbRemove->setText(QCoreApplication::translate("TabWidget", "Remove", nullptr));
        radioNoText->setText(QCoreApplication::translate("TabWidget", "Add no text", nullptr));
        pbClose->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tabFileName), QCoreApplication::translate("TabWidget", "File Name Pattern", nullptr));
        lblMyFileExtensions->setText(QCoreApplication::translate("TabWidget", "Custom file extensions, patterns", nullptr));
        pbNewExtension->setText(QCoreApplication::translate("TabWidget", "New", nullptr));
        pbRemoveExtension->setText(QCoreApplication::translate("TabWidget", "Remove", nullptr));
        pbDefaultExtension->setText(QCoreApplication::translate("TabWidget", "Default", nullptr));
        lblMyFileExtensionsPattern->setText(QCoreApplication::translate("TabWidget", "Name [*.extension1 *.extension2] (*.extension1 *.extension2)", nullptr));
        lblMyFileExtension2->setText(QCoreApplication::translate("TabWidget", "Filter by file extension at \"Rename\" and \"Copy\"", nullptr));
        pbCloseExtension->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        lblMyFileExtensions_2->setText(QCoreApplication::translate("TabWidget", "Be careful if you add your own file extensions!", nullptr));
        pbNewExtension_2->setText(QCoreApplication::translate("TabWidget", "New", nullptr));
        pbRemoveExtension_2->setText(QCoreApplication::translate("TabWidget", "Remove", nullptr));
        pbDefaultExtension_2->setText(QCoreApplication::translate("TabWidget", "Default", nullptr));
        lblMyFileExtension2_2->setText(QCoreApplication::translate("TabWidget", "Allowed file extensions", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tabExtensions), QCoreApplication::translate("TabWidget", "Extensions", nullptr));
        lblSetLocation->setText(QCoreApplication::translate("TabWidget", "Enter a default location to copy to", nullptr));
        pbSetLocation->setText(QCoreApplication::translate("TabWidget", "Select", nullptr));
        lblCopyLocation->setText(QString());
        radioDontCopyToDefault->setText(QCoreApplication::translate("TabWidget", "Do not copy to default location", nullptr));
        radioCopyToDefault->setText(QCoreApplication::translate("TabWidget", "Always copy to default location without asking", nullptr));
        pbCloseOpenCopy->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        radioLastUsed->setText(QCoreApplication::translate("TabWidget", "Always open the last selected folder", nullptr));
        radioHome->setText(QCoreApplication::translate("TabWidget", "RadioButton", nullptr));
        radioDragDroppRename->setText(QCoreApplication::translate("TabWidget", "Drag and drop renames the files", nullptr));
        radioDragDroppCopy->setText(QCoreApplication::translate("TabWidget", "Drag and drop copies the files", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tabOpenCopy), QCoreApplication::translate("TabWidget", "Open / Copy to", nullptr));
        checkDoCheckOnStart->setText(QCoreApplication::translate("TabWidget", "Check for updates when the program starts", nullptr));
        checkNativeDialog->setText(QCoreApplication::translate("TabWidget", "Use the operating system's own dialogs", nullptr));
        pbCloseMiscellaneous->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        checkChortcutApplicationsLocation->setText(QCoreApplication::translate("TabWidget", "Shortcut in the operating system menu", nullptr));
        checkChortcutDesktopLocation->setText(QCoreApplication::translate("TabWidget", "Desktop shortcut", nullptr));
        checkToolTips->setText(QCoreApplication::translate("TabWidget", "Show tooltips", nullptr));
        pbForceUpdate->setText(QCoreApplication::translate("TabWidget", "Force Update", nullptr));
        checkDisplayFailure->setText(QCoreApplication::translate("TabWidget", "Display Failure", nullptr));
        checkDisplaySuccess->setText(QCoreApplication::translate("TabWidget", "Display Success", nullptr));
        checkRedText->setText(QCoreApplication::translate("TabWidget", "Failures are written in red text", nullptr));
        checkActionRename->setText(QCoreApplication::translate("TabWidget", "Rename", nullptr));
        lblToolbar->setText(QCoreApplication::translate("TabWidget", "Choose what you want to display in the toolbar", nullptr));
        checkActionClose->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        checkActionRenameRecursively->setText(QCoreApplication::translate("TabWidget", "Rename recursively", nullptr));
        checkActionCopyRename->setText(QCoreApplication::translate("TabWidget", "Copy and Rename", nullptr));
        checkActionClear->setText(QCoreApplication::translate("TabWidget", "Clear", nullptr));
        checkActionEditExif->setText(QCoreApplication::translate("TabWidget", "Edit EXIF Date/Time", nullptr));
        checkActionEditExifRename->setText(QCoreApplication::translate("TabWidget", "Edit EXIF Date/Time and Rename", nullptr));
        checkActionEditExifOffset->setText(QCoreApplication::translate("TabWidget", "Edit EXIF Date/Time with offset", nullptr));
        checkActionEditExifOffsetRename->setText(QCoreApplication::translate("TabWidget", "Edit EXIF Date/Time with offset and Rename", nullptr));
        pbTestLanguagefile->setText(QCoreApplication::translate("TabWidget", "Pre-check language file", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tabMiscellaneous), QCoreApplication::translate("TabWidget", "Misc.", nullptr));
        pbCloseLicense->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tabLicense), QCoreApplication::translate("TabWidget", "License", nullptr));
        pbCloseAbout->setText(QCoreApplication::translate("TabWidget", "Close", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tabAbout), QCoreApplication::translate("TabWidget", "About", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TabWidget: public Ui_TabWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABWIDGET_H

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "tabwidget.h"
#include "mainwindow.h"
#include "ui_tabwidget.h"

void TabWidget::chortcutdesktop(int state)
{
    const QString shortcutlocation = QDir::toNativeSeparators(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));
#ifdef Q_OS_WIN
    QString link(QDir::toNativeSeparators(shortcutlocation + "/EXIF ReName.lnk"));
#endif
#ifdef Q_OS_LINUX
    QString link(QDir::toNativeSeparators(shortcutlocation + "/exifrename.desktop"));
#endif

    if(state == 2) {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }

        if(!makeDesktopFile(link)) {
            ui->checkChortcutDesktopLocation->setCheckState(Qt::Unchecked);
        }
    } else {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }
    }
}

void TabWidget::chortcutapplications(int state)
{
    const QString shortcutlocation = QDir::toNativeSeparators(QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation));
#ifdef Q_OS_WIN
    QString link(QDir::toNativeSeparators(shortcutlocation + "/EXIF ReName.lnk"));
#endif
#ifdef Q_OS_LINUX
    QString link(QDir::toNativeSeparators(shortcutlocation + "/exifrename.desktop"));
    QDir dir(shortcutlocation);

    if(!dir.mkpath(shortcutlocation)) {
        QMessageBox::critical(
            this, DISPLAY_NAME " " VERSION,
            tr("Failure!\nThe shortcut could not be created in\n\"~/.local/share/applications\"\nCheck your file permissions."));
        return;
    }

#endif

    if(state == 2) {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }

        if(!makeDesktopFile(link)) {
            ui->checkChortcutApplicationsLocation->setCheckState(Qt::Unchecked);
        }
    } else {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }
    }
}

bool TabWidget::makeDesktopFile(QString path)
{
    QFile file(path);
#ifdef Q_OS_LINUX
    const QString source = QDir::toNativeSeparators(qgetenv("APPIMAGE"));
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    const QString iconpath = QDir::toNativeSeparators(fi.absolutePath() + "/exifrename.png");
#endif
#ifdef Q_OS_WIN
    const QString source = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/exifrename.exe");
#endif
#ifdef Q_OS_LINUX

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);

        if(QFile::exists(iconpath)) {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=EXIF ReName\nIcon=" + iconpath + "\nTerminal=false\nComment=" + tr("Rename image files so that the name contains the day and time the image was taken") + "\nExec=\"" + source + "\"\nCategories=Graphics;\n";
        } else {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=EXIF ReName\nTerminal=false\nComment=" + tr("Rename image files so that the name contains the day and time the image was taken") + "\nExec=\"" + source + "\"\nCategories=Graphics;\n";
        }

        file.close();
        file.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                            QFileDevice::WriteUser | QFileDevice::ReadOther |
                            QFileDevice::ExeOther);
        return true;
    }

#endif
#ifdef Q_OS_WIN

    if(QFile::link(QDir::toNativeSeparators(source), QDir::toNativeSeparators(path))) {
        return true;
    }

#endif
    else {
        QMessageBox::critical(
            this, DISPLAY_NAME " " VERSION,
            tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
        return false;
    }

    return true;
}

bool TabWidget::chortcutExists(QString path)
{
    QFileInfo file(path);

    if(file.exists()) {
        return true;
    } else {
        return false;
    }
}

bool TabWidget::chortcutIsExecutable(QString path)
{
    QFileInfo file(path);

    if(file.isExecutable()) {
        return true;
    } else {
        return false;
    }
}

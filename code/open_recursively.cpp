//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "tabwidget.h"
#include "mainwindow.h"
#include "ui_tabwidget.h"
#include "ui_mainwindow.h"
void MainWindow::openFileRecursively()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    QStringList selectedextensions = settings.value("selectedextensions").toStringList();
    settings.endGroup();
    QDir dir(QDir::home());
    QString home = QDir::toNativeSeparators(dir.absolutePath());
    settings.beginGroup("Path");
    QString directorypath = settings.value("directorypath", home).toString();
    settings.endGroup();
    QFileDialog * dialog = new QFileDialog(this);
    dialog->resize(900, 600);
    // dialog->setFileMode(QFileDialog::DirectoryOnly);
    dialog->setFileMode(QFileDialog::Directory);
    settings.beginGroup("Tools");
    dialog->setOption(QFileDialog::DontUseNativeDialog, !settings.value("usenativedialog").toBool());
    bool useopenpath = settings.value("useopenpath", false).toBool();
    settings.endGroup();

    if(useopenpath) {
        dialog->setDirectory(directorypath);
    } else {
        dialog->setDirectory(home);
    }

    if(dialog->exec()) {
        QDir directory = dialog->directory();
        directorypath = directory.path();
    } else {
        return;
    }

    QStringList filelist;
    QStringList filter;

    foreach(QString defaultfilter, Filters::allExtensions()) {
        filter << ("*." + defaultfilter);
    }

    QDirIterator it(directorypath, filter, QDir::Files, QDirIterator::Subdirectories);
    QString next;

    while(it.hasNext()) {
        next = it.next();
        QFileInfo fi(next);

        if(fi.isWritable()) {
            filelist << next;
        }
    }

    findExif(filelist);
    settings.beginGroup("Path");
    settings.setValue("directorypath", directorypath);
    settings.endGroup();
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGuiApplication>
#include <QScreen>
void MainWindow::readPlaceonscreen()
{
#ifdef Q_OS_WIN // Windows 32- and 64-bit
#ifdef portable
    ui->actionUpdate->setVisible(false);
#endif
#ifndef portable
    ui->actionUpdate->setText(tr("Update / Uninstall"));
    ui->actionUpdate->setEnabled(true);
#endif
#endif
    QString fontPathRegular = ":/fonts/Ubuntu-R.ttf";
    int fontidRegular = QFontDatabase::addApplicationFont(fontPathRegular);
    QString regular = QFontDatabase::applicationFontFamilies(fontidRegular).at(0);
    QFont fregular(regular, FONTSIZE, QFont::Normal);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Font");
    int size = settings.value("size", FONTSIZE).toInt();
    QFont font = settings.value("font", fregular).value<QFont>();
    settings.endGroup();
    font.setPointSize(size);
    ui->textBrowserMain->setFont(font);
    settings.beginGroup("MainWindow");
    QPoint punkt = settings.value("pos", QPoint(200, 200)).toPoint();
    this->move(settings.value("pos", QPoint(200, 200)).toPoint());
    QSize storlek = settings.value("size", QSize(700, 300)).toSize();
    this->resize(settings.value("size", QSize(700, 300)).toSize());
    settings.endGroup();
    int widd = 0, hojd = 0;

    foreach(QScreen *screen, QGuiApplication::screens()) {
        widd = screen->size().width();
        hojd = screen->size().height();

        if((storlek.width() >= widd) || (storlek.height() >= hojd)) {
            this->resize(widd, hojd);
            this->setWindowState(Qt::WindowMaximized);
        }

        if((punkt.x() > widd - 100) || (punkt.y() > hojd - 100) || punkt.x() < 0 ||
                punkt.y() < 0) {
            this->move(200, 200);
            break;
        }
    }

#ifdef Q_OS_LINUX
    settings.beginGroup("Updates");
    bool updateneeded = settings.value("updateneeded", false).toBool();
    settings.endGroup();

    if(updateneeded) {
        ui->actionUpdate->setEnabled(true);
    } else {
        ui->actionUpdate->setDisabled(true);
    }

#endif
    getCloseSignalTabWidget();
}

void MainWindow::writePlaceonscreen()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("MainWindow");
    settings.setValue("size", this->size());
    settings.setValue("pos", this->pos());
    settings.endGroup();
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "exif.h"
#include <fstream>
#include <QFile>
#include <QDir>
#include <stdio.h>

const int LAS_LANGD = 5120;
// 3072
using namespace std;
Exif::Exif() {}

bool Exif::getExif(QString &filnamn, char *hittat, QString *info)
{
    QFile lasfil(filnamn);
    char in;
    char hela[LAS_LANGD] = {};

    if(!lasfil.open(QIODevice::ReadOnly | QIODevice::Text)) {
        *info = tr("Unable to open") + " \"" + QDir::toNativeSeparators(filnamn) + "\"";
        lasfil.close();
        return false;
    }

    for(int i = 0; i < LAS_LANGD; i++) {
        if(lasfil.read((char *)&in, 1)) {
            hela[i] = in;
        } else {
            *info = tr("No EXIF data found in")  + " \"" + QDir::toNativeSeparators(filnamn) + "\"";
            lasfil.close();
            return false;
        }
    }

    finn(hela, hittat);

    if(strcmp(hittat, "") == 0) {
        *info = tr("No EXIF data found in") + " \"" + QDir::toNativeSeparators(filnamn) + "\"";
        lasfil.close();
        return false;
    }

    format_good(hittat);
    *info = tr("EXIF data found in") + " \"" + QDir::toNativeSeparators(filnamn) + "\"";
    lasfil.close();
    return true;
}
void Exif::finn(char *h, char *hittat)
{
    int k = 0;

    for(int i = 0; i <= LAS_LANGD; i++) {
        if(s(h[i]) && s(h[i + 1]) && s(h[i + 2]) && s(h[i + 3]) && s(h[i + 5]) &&
                sa(h[i + 4]) && s(h[i + 5]) && s(h[i + 6]) && sa(h[i + 7]) &&
                s(h[i + 8]) && s(h[i + 9]) && sb(h[i + 10]) && s(h[i + 11]) &&
                s(h[i + 12]) && sa(h[i + 13]) && s(h[i + 14]) && s(h[i + 15]) &&
                sa(h[i + 16]) && s(h[i + 17]) && s(h[i + 18])) {
            k++;

            if(k == 1) {
                for(int k = 0; k < 19; k++, i++) {
                    hittat[k] = h[i];
                }
            }

            if(k > 1) {
                for(int k = 0; k < 19; k++, i++) {
                    hittat[k] = h[i];
                }

                return;
            }
        }
    }
}

bool Exif::s(char c)
{
    if(c >= '0' && c <= '9')
        return true;

    return false;
}
bool Exif::sa(char c)
{
    if(c == ':')
        return true;

    return false;
}
bool Exif::sb(char c)
{
    if(c == ' ')
        return true;

    return false;
}

void Exif::format_good(char *s)
{
    s[4] = 'x';
    s[7] = 'x';
    s[10] = 'x';
    s[13] = 'x';
    s[16] = 'x';
    s[19] = '\0';
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "tabwidget.h"
#include "mainwindow.h"
#include "ui_tabwidget.h"
#include "ui_mainwindow.h"

void MainWindow::openFile()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Extensions");
    QStringList selectedfilters = settings.value("selectedfilters").toStringList();
    selectedfilters.sort(Qt::CaseInsensitive);
    settings.endGroup();
    QDir dir(QDir::home());
    QString home = dir.absolutePath();
    settings.beginGroup("Path");
    QString openpath = settings.value("openpath", home).toString();
    settings.endGroup();
    QFileDialog * dialog = new QFileDialog(this);
    dialog->resize(900, 600);
    dialog->setViewMode(QFileDialog::Detail);
    dialog->setFileMode(QFileDialog::ExistingFiles);
    settings.beginGroup("Tools");
    dialog->setWindowTitle(tr("Select file or files to rename"));
    dialog->setOption(QFileDialog::DontUseNativeDialog, !settings.value("usenativedialog").toBool());
    bool useopenpath = settings.value("useopenpath", false).toBool();
    settings.endGroup();
    settings.beginGroup("Extensions");
    QString selectednamefilter = settings.value("selectednamefilter", "tomt").toString();
    settings.endGroup();

    if(useopenpath) {
        if(!TabWidget::pathExist(openpath)) {
            dialog->setDirectory(home);
        } else if(openpath.isEmpty()) {
            dialog->setDirectory(home);
        } else {
            dialog->setDirectory(openpath);
        }
    } else {
        dialog->setDirectory(home);
    }

    QStringList fileNames;

    if(selectednamefilter != "tomt") {
        dialog->setNameFilters(QStringList() << selectednamefilter << selectedfilters);
    } else {
        dialog->setNameFilters(selectedfilters);
    }

    if(dialog->exec()) {
        fileNames = dialog->selectedFiles();
        findExif(fileNames);
        QDir dir = dialog->directory();
        openpath = dir.path();
        settings.beginGroup("Path");
        settings.setValue("openpath", openpath);
        settings.endGroup();
        settings.beginGroup("Extensions");
        settings.setValue("selectednamefilter", dialog->selectedNameFilter());
        settings.endGroup();
    }
}

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef INPUTDIALOGOFFSET_H
#define INPUTDIALOGOFFSET_H
//#include "inputdialog_global.h"
#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QDebug>
#include <QDateTime>
//class INPUTDIALOG_EXPORT InputDialog : public QDialog
class InputDialogOffset : public QDialog
{
    Q_OBJECT

public:
    InputDialogOffset(QWidget *parent = 0);
    ~InputDialogOffset();
    void getOldDateTime(QString olddatetime);

signals:
    void setOffset(QString datetime);


public slots:
    void getNewValue(QString olddatetime);

private:
    QLineEdit * le;
    QLabel * lblInfo;
    QString regular;
    QString OFFSET;
    QPushButton *okButton;
};
#endif // INPUTDIALOGOFFSET_H

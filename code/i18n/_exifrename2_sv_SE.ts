<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>DialogOffset</name>
    <message>
        <location filename="../dialogoffset.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
</context>
<context>
    <name>Exif</name>
    <message>
        <location filename="../exif.cpp" line="37"/>
        <source>Unable to open</source>
        <translation>Kan inte öppna</translation>
    </message>
    <message>
        <location filename="../exif.cpp" line="46"/>
        <location filename="../exif.cpp" line="55"/>
        <source>No EXIF data found in</source>
        <translation>Hittar ingen EXIF-data i</translation>
    </message>
    <message>
        <location filename="../exif.cpp" line="61"/>
        <source>EXIF data found in</source>
        <translation>EXIF data hittat i</translation>
    </message>
</context>
<context>
    <name>Exifw</name>
    <message>
        <location filename="../exifw.cpp" line="38"/>
        <source>Unable to open</source>
        <translation>Kan inte öppna</translation>
    </message>
    <message>
        <location filename="../exifw.cpp" line="62"/>
        <source>No EXIF data found in</source>
        <translation>Hittar ingen EXIF-data i</translation>
    </message>
    <message>
        <location filename="../exifw.cpp" line="69"/>
        <source>Unable to write EXIF data to</source>
        <translation>Kan inte skriva EXIF data till</translation>
    </message>
    <message>
        <location filename="../exifw.cpp" line="77"/>
        <source>Wrote EXIF data to</source>
        <translation>Skrev EXIF data till</translation>
    </message>
    <message>
        <location filename="../exifw.cpp" line="133"/>
        <source>Unable to write to</source>
        <translation>Kan inte skriva till</translation>
    </message>
</context>
<context>
    <name>Filters</name>
    <message>
        <location filename="../filters.h" line="39"/>
        <source>All Files</source>
        <translation>Alla filer</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="23"/>
        <source>A program to name pictures after the date and time the picture was taken.</source>
        <translation>Ett program för att namnge bilder efter datum och tid då bilden togs.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="47"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="48"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="61"/>
        <location filename="../info.cpp" line="70"/>
        <location filename="../info.cpp" line="79"/>
        <location filename="../info.cpp" line="173"/>
        <source> Compiled by</source>
        <translation> Kompilerad med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="93"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="169"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="180"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilator.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="200"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="202"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="204"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="206"/>
        <source>Downloads</source>
        <translation>Nedladdningar</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="209"/>
        <source>Phone: </source>
        <translation>Tfn: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="213"/>
        <source>This program uses Qt version </source>
        <translation>Detta program använder Qt version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="214"/>
        <source> running on </source>
        <translation> som körs på </translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <location filename="../inputdialog.cpp" line="52"/>
        <source>For example</source>
        <translation>Till exempel</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="52"/>
        <source>Year-Month-day Hour:Minute:Second (4 digits for Year, 2 digits for Month, Day, Hour, Minute and Second)</source>
        <translation>År-Månad-Dag Timme:Minut:Sekund (4 siffror för År, 2 siffror för Månad, Dag, Timme, Minut och Sekund)</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="52"/>
        <source>Type in new Date and Time. You must type exactly like this:</source>
        <translation>Skriv in Datum och Tid. Du måste skriva precis så här:</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="52"/>
        <source>(The image&apos;s current EXIF data for Date and Time.)</source>
        <translation>(Bildens nuvarande EXIF data för Datum och Tid.)</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="85"/>
        <source>Not correct. Four digits for year. Two digits for month, day, hour, minute and second.</source>
        <translation>Inte korrekt. Fyra siffror för året. Två siffror för månad, dag, timme, minut och sekund.</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="99"/>
        <source>This date and this time does not exist.</source>
        <translation>Detta datum och denna tid finns inte.</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="104"/>
        <source>This date does not exist.</source>
        <translation>Detta datum finns inte.</translation>
    </message>
    <message>
        <location filename="../inputdialog.cpp" line="109"/>
        <source>This time does not exist.</source>
        <translation>Den här tiden finns inte.</translation>
    </message>
</context>
<context>
    <name>InputDialogOffset</name>
    <message>
        <location filename="../inputdialogoffset.cpp" line="51"/>
        <source>Year-Month-day Hour:Minute:Second (4 digits for Year, 2 digits for Month, Day, Hour, Minute and Second)</source>
        <translation>År-Månad-Dag Timme:Minut:Sekund (4 siffror för År, 2 siffror för Månad, Dag, Timme, Minut och Sekund)</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="51"/>
        <source>For example</source>
        <translation>Till exempel</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="51"/>
        <source>Type in correct Date and Time. You must type exactly like this:</source>
        <translation>Skriv in korrekt datum och tid. Du måste skriva exakt så här:</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="51"/>
        <source>(The image&apos;s current EXIF data for Date and Time.)</source>
        <translation>(Bildens nuvarande EXIF data för Datum och tid.)</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="65"/>
        <source>&amp;Calculate</source>
        <translation>&amp;Beräkna</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="70"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="72"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="100"/>
        <source>Not correct. Four digits for year. Two digits for month, day, hour, minute and second.</source>
        <translation>Inte korrekt. Fyra siffror för året. Två siffror för månad, dag, timme, minut och sekund.</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="114"/>
        <source>This date and this time does not exist.</source>
        <translation>Detta datum och denna tid finns inte.</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="119"/>
        <source>This date does not exist.</source>
        <translation>Detta datum finns inte.</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="124"/>
        <source>This time does not exist.</source>
        <translation>Den här tiden finns inte.</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="135"/>
        <source>Offset calculated to</source>
        <translation>Offset beräknat till</translation>
    </message>
    <message>
        <location filename="../inputdialogoffset.cpp" line="135"/>
        <source>seconds. This offset is used on the images then you choose to change EXIF Date and Time.</source>
        <translation>sekunder. Denna förskjutning används på bilderna när du väljer att ändra EXIF Datum och Tid.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Huvudfönster</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="91"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="101"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="121"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="128"/>
        <source>View</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="135"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="145"/>
        <source>&amp;Edit</source>
        <translation>&amp;Redigera</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="162"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="178"/>
        <source>File name pattern...</source>
        <translation>Filnamnsmönster...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="188"/>
        <source>Check for updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="196"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="205"/>
        <source>Svenska</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="208"/>
        <source>Swedish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="217"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="220"/>
        <source>Engelska</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="225"/>
        <source>Rename...</source>
        <translation>Namnändra...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="228"/>
        <source>The file will be named after its EXIF Date/Time. If you
specified this, your selected text string will also be added
 to the file name.</source>
        <translation>Filen kommer att namnges efter sin EXIF Datum/Tid.
Om du angett detta kommer din valda textsträng
också att läggas till filnamnet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="233"/>
        <source>The file will be named after its EXIF Date/Time. If you specified this, your selected text string will also be added to the file name.</source>
        <translation>Filen kommer att namnges efter sin EXIF Datum/Tid. Om du angett detta kommer din valda textsträng också att läggas till filnamnet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="238"/>
        <source>Extensions...</source>
        <translation>Filnamnstillägg...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="243"/>
        <source>License...</source>
        <translation>Licens...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="248"/>
        <source>Open / Copy to...</source>
        <translation>Öppna / Kopiera till...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="253"/>
        <source>Copy and Rename...</source>
        <translation>Kopiera och namnändra...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>The files you select will be copied to the location you selected.
Subfolders are created according to your settings.
The files are renamed according to your settings.</source>
        <translation>Filerna du väljer kommer att kopieras till den plats du valt.
Undermappar skapas enligt dina inställningar. Filerna namnändras
efter dina inställningar.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="261"/>
        <source>The files you select will be copied to the location you selected. Subfolders are created according to your settings. The files are renamed according to your settings.</source>
        <translation>Filerna du väljer kommer att kopieras till den plats du valt. Undermappar skapas enligt dina inställningar. Filerna namnändras efter dina inställningar.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="303"/>
        <source>Rename recursively...</source>
        <translation>Nämnändra rekursivt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="306"/>
        <source>All files in the folder you select and in all subfolders will be
renamed if they contain EXIF data. Each file will be renamed
according to its EXIF Date/Time. If you specified a text
string to be included in the file name, it will be added.</source>
        <translation>Alla filer i mappen du väljer och i alla undermappar kommer
att byta namn om de innehåller EXIF-data. Varje fil får namn
enligt sin EXIF Datum/Tid. Om du angav en textsträng som
ska ingå i filnamnet läggs den till.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="312"/>
        <source>All files in the folder you select and in all subfolders will be renamed if they contain EXIF data. Each file will be renamed according to its EXIF Date/Time. If you specified a text string to be included in the file name, it will be added.</source>
        <translation>Alla filer i mappen du väljer och i alla undermappar kommer att finnas bytt namn om de innehåller EXIF-data. Varje fil byts namn enligt EXIF Datum/Tid. Om du angav en textsträng som ska ingå i filnamnet läggs den till.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>EXIF Date/Time...</source>
        <translation>EXIF Datum/Tid...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="332"/>
        <source>EXIF Date/Time and Rename...</source>
        <translation>EXIF Datum/Tid och namnändra...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="337"/>
        <source>EXIF Date/Time with offset...</source>
        <translation>EXIF Datum/Tid från offset...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="342"/>
        <source>EXIF Date/Time with offset and Rename...</source>
        <translation>EXIF Datum/Tid från offset och namnändra...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Miscellaneous...</source>
        <translation>Diverse...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="274"/>
        <source>In +</source>
        <translation>In +</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <location filename="../mainwindow.ui" line="285"/>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Zoom with Ctrl + Mouse Wheel</source>
        <translation>Zoom med Ctrl + Mushjul</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="282"/>
        <source>Out -</source>
        <translation>Ut -</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="290"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="298"/>
        <source>Delete all settings...</source>
        <translation>Ta bort alla inställningar...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="317"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="322"/>
        <source>Font...</source>
        <translation>Teckensnitt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="76"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="78"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="199"/>
        <source>Do you want to delete all saved settings and shortcuts?
They cannot be restored.</source>
        <translation>Vill du ta bort alla sparade inställningar och genvägar?
De går inte att återställa.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="199"/>
        <source>closes.</source>
        <translation>stängs.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="202"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="201"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <source>Maintenance Tool cannot be found.
Only if you install </source>
        <translation>Underhållsverktyget kan inte hittas.
Endast om du installerar </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="194"/>
        <source> is it possible to update and uninstall the program.</source>
        <translation> är det möjligt att uppdatera och avinstallera programmet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="230"/>
        <source>Failed to delete your configuration files. Check your file permissions.</source>
        <translation>Det gick inte att ta bort konfigurationsfilerna. Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="421"/>
        <source>An unexpected error occurred while updating.</source>
        <translation>Ett oväntat fel inträffade vid uppdateringen.</translation>
    </message>
    <message>
        <location filename="../langguage.cpp" line="39"/>
        <location filename="../langguage.cpp" line="73"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska träda i kraft.</translation>
    </message>
    <message>
        <location filename="../langguage.cpp" line="36"/>
        <location filename="../langguage.cpp" line="70"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../langguage.cpp" line="35"/>
        <location filename="../langguage.cpp" line="69"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../findexif.cpp" line="68"/>
        <location filename="../findexif.cpp" line="71"/>
        <location filename="../open_copy.cpp" line="165"/>
        <location filename="../open_copy.cpp" line="168"/>
        <source>Incorrect file name extension.</source>
        <translation>Felaktigt filnamnstillägg.</translation>
    </message>
    <message>
        <location filename="../findexif.cpp" line="85"/>
        <location filename="../findexif.cpp" line="87"/>
        <source>Trying to rename</source>
        <translation>Försöker att ändra namn på</translation>
    </message>
    <message>
        <location filename="../change_exif_manyfiles.cpp" line="43"/>
        <source>Select files to adjust EXIF Date/Time according to offset</source>
        <translation>Välj filer för att justera EXIF Datum/Tid enligt offset</translation>
    </message>
    <message>
        <location filename="../change_exif_manyfiles.cpp" line="47"/>
        <source>Select file or files to be modified by offset</source>
        <translation>Välj fil eller filer som skall ändras av offset</translation>
    </message>
    <message>
        <location filename="../change_exif_manyfiles.cpp" line="84"/>
        <location filename="../change_exif_manyfiles.cpp" line="86"/>
        <source>Attempting to modify EXIF Date/Time in</source>
        <translation>Försöker att modifiera EXIF Datum/Tid i</translation>
    </message>
    <message>
        <location filename="../change_exif_manyfiles.cpp" line="86"/>
        <location filename="../change_exif_manyfiles.cpp" line="191"/>
        <location filename="../findexif.cpp" line="85"/>
        <location filename="../findexif.cpp" line="150"/>
        <location filename="../findexif.cpp" line="156"/>
        <location filename="../open_copy.cpp" line="154"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../change_exif_manyfiles.cpp" line="189"/>
        <location filename="../change_exif_manyfiles.cpp" line="191"/>
        <source>Managed to change EXIF Date/Time in</source>
        <translation>Lyckades att modifiera EXIF Datum/Tid i</translation>
    </message>
    <message>
        <location filename="../change_exif_manyfiles.cpp" line="84"/>
        <location filename="../change_exif_manyfiles.cpp" line="189"/>
        <location filename="../findexif.cpp" line="87"/>
        <location filename="../findexif.cpp" line="152"/>
        <location filename="../findexif.cpp" line="158"/>
        <location filename="../open_copy.cpp" line="156"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../change_exif.cpp" line="43"/>
        <source>Select file to get new EXIF Date/Time</source>
        <translation>Välj fil som ska få nytt EXIF Datum/Tid</translation>
    </message>
    <message>
        <location filename="../change_exif.cpp" line="89"/>
        <location filename="../change_exif.cpp" line="92"/>
        <location filename="../change_exif.cpp" line="101"/>
        <location filename="../change_exif.cpp" line="104"/>
        <location filename="../change_exif_manyfiles.cpp" line="105"/>
        <location filename="../change_exif_manyfiles.cpp" line="108"/>
        <location filename="../change_exif_manyfiles.cpp" line="119"/>
        <location filename="../change_exif_manyfiles.cpp" line="122"/>
        <location filename="../change_exif_offset.cpp" line="90"/>
        <location filename="../change_exif_offset.cpp" line="93"/>
        <location filename="../change_exif_offset.cpp" line="102"/>
        <location filename="../change_exif_offset.cpp" line="105"/>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="90"/>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="93"/>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="102"/>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="105"/>
        <location filename="../open_copy.cpp" line="193"/>
        <location filename="../open_copy.cpp" line="196"/>
        <source>No EXIF data found in</source>
        <translation>Hittar ingen EXIF-data i</translation>
    </message>
    <message>
        <location filename="../change_exif.cpp" line="161"/>
        <source>changed to</source>
        <translation>ändrades till</translation>
    </message>
    <message>
        <location filename="../findexif.cpp" line="139"/>
        <source>Managed to rename all files</source>
        <translation>Lyckades att namnändra alla filer</translation>
    </message>
    <message>
        <location filename="../findexif.cpp" line="143"/>
        <location filename="../findexif.cpp" line="146"/>
        <source>Failed to rename any file</source>
        <translation>Lyckades inte ändra namn på någon fil</translation>
    </message>
    <message>
        <location filename="../findexif.cpp" line="163"/>
        <location filename="../findexif.cpp" line="166"/>
        <source>Failed to rename</source>
        <translation>Misslyckades att namnändra</translation>
    </message>
    <message>
        <location filename="../findexif.cpp" line="163"/>
        <location filename="../findexif.cpp" line="166"/>
        <source>and managed to rename</source>
        <translation>och lyckades att namnändra</translation>
    </message>
    <message>
        <location filename="../rename.cpp" line="49"/>
        <location filename="../rename.cpp" line="52"/>
        <location filename="../rename.cpp" line="67"/>
        <location filename="../rename.cpp" line="70"/>
        <source>The operating system does not allow you to change the file name</source>
        <translation>Operativsystemet tillåter inte att du ändrar filnamnet</translation>
    </message>
    <message>
        <location filename="../rename.cpp" line="163"/>
        <source>Path and new name is</source>
        <translation>Sökväg och nytt namn är</translation>
    </message>
    <message>
        <location filename="../rename.cpp" line="172"/>
        <source>retains its original name</source>
        <translation>behåller sitt ursprungliga namn</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="29"/>
        <source>Update / Uninstall</source>
        <translation>Uppdattera / Avinstallera</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="40"/>
        <source>Select file or files to be copied and renamed</source>
        <translation>Välj fil eller filer som skall kopieras och namnändras</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="154"/>
        <location filename="../open_copy.cpp" line="156"/>
        <source>Trying to copy</source>
        <translation>Försöker att kopiera</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="233"/>
        <location filename="../open_copy.cpp" line="235"/>
        <location filename="../open_copy.cpp" line="241"/>
        <location filename="../open_copy.cpp" line="243"/>
        <source>The file cannot overwrite itself. You are trying to copy</source>
        <translation>Filen kan inte skriva över sig själv. Du försöker kopiera</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="233"/>
        <location filename="../open_copy.cpp" line="241"/>
        <source>file to itself.</source>
        <translation>fil till sig själv.</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="235"/>
        <location filename="../open_copy.cpp" line="243"/>
        <source>files to itself.</source>
        <translation>filer till sig själva.</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="254"/>
        <location filename="../open_copy.cpp" line="257"/>
        <source>Check your file permissions.</source>
        <translation>Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="254"/>
        <location filename="../open_copy.cpp" line="257"/>
        <source>Prohibited from copying to:</source>
        <translation>Förbjudet att kopiera till:</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="270"/>
        <source>No file was copied</source>
        <translation>Ingen fil kopierades</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="275"/>
        <source>file was copied</source>
        <translation>fil kopierades</translation>
    </message>
    <message>
        <location filename="../open_copy.cpp" line="277"/>
        <source>files was copied</source>
        <translation>filer kopierades</translation>
    </message>
    <message>
        <location filename="../firstrun.cpp" line="72"/>
        <source>Welcome to EXIF ReName!</source>
        <translation>Välkommen till EXIF ReName!</translation>
    </message>
    <message>
        <location filename="../firstrun.cpp" line="73"/>
        <source>Before you begin, please take a look at the settings.</source>
        <translation>Innan du börjar, vänligen ta en titt på inställningarna.</translation>
    </message>
    <message>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="151"/>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="154"/>
        <source>No file with EXIF data found, cannot rename any file</source>
        <translation>Ingen fil med EXIF data hittades, kan inte namnändra någon fil</translation>
    </message>
    <message>
        <location filename="../open.cpp" line="42"/>
        <source>Select file or files to rename</source>
        <translation>Välj fil eller filer att namnändra</translation>
    </message>
    <message>
        <location filename="../change_exif_offset.cpp" line="44"/>
        <location filename="../change_exif_offset_manyfiles_rename.cpp" line="44"/>
        <source>Select file to set offset</source>
        <translation>Välj fil för att ställa in offset</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../updates.cpp" line="69"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../updates.cpp" line="74"/>
        <source>Download a new</source>
        <translation>Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../updates.cpp" line="78"/>
        <source>Select &quot;Tools&quot;, &quot;Update / Uninstall&quot; and &quot;Update component&quot;.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera / avinstallera&quot; och &quot;Update component&quot;.</translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="85"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="95"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="102"/>
        <source>Italic</source>
        <translation>Rekursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="109"/>
        <source>Bold and italic</source>
        <translation>Fet och kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="138"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="148"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="59"/>
        <source>All fonts</source>
        <translation>Alla fonter</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="59"/>
        <source>Monospaced fonts</source>
        <translation>Monospaced teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="60"/>
        <source>Proportional fonts</source>
        <translation>Proportionella teckensnitt</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../tabwidget.ui" line="23"/>
        <source>TabWidget</source>
        <translation>TabWidget</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="58"/>
        <source>File Name Pattern</source>
        <translation>Filnamnsmönster</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="70"/>
        <source>lblCurrentDateTime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="123"/>
        <location filename="../tabwidget.ui" line="238"/>
        <location filename="../tabwidget.ui" line="330"/>
        <source>Space</source>
        <translation>Mellanslag</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="128"/>
        <location filename="../tabwidget.ui" line="243"/>
        <location filename="../tabwidget.ui" line="335"/>
        <source>Hyphen -</source>
        <translation>Bindesträck -</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="133"/>
        <location filename="../tabwidget.ui" line="248"/>
        <location filename="../tabwidget.ui" line="340"/>
        <source>Underscore _</source>
        <translation>Understräck _</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="156"/>
        <source>File name figures,&lt;br&gt;separeted by</source>
        <translation>Siffrorna i filnamnet&lt;br&gt;avdelas av</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="194"/>
        <source>Folder name figures,&lt;br&gt;separeted by</source>
        <translation>Siffrorna i mappnamnet&lt;br&gt;avdelas av</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="292"/>
        <source>Date and Time,&lt;br&gt;separeted by</source>
        <translation>Datum och tid&lt;br&gt;avdelas av</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="355"/>
        <source>File name:</source>
        <translation>Filnamn:</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="368"/>
        <source>Folder name:</source>
        <translation>Mappnamn:</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="381"/>
        <source>lblFolder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="409"/>
        <source>Extension</source>
        <translation>Filnamnstillägg</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="425"/>
        <source>CAPITAL</source>
        <translation>VERSALER</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="438"/>
        <source>lowercase</source>
        <translation>gemener</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="560"/>
        <source>Add text to the file name</source>
        <translation>Lägg till text till filnamnet</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="573"/>
        <source>In the beginning</source>
        <translation>I början</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="647"/>
        <source>Add no text</source>
        <translation>Lägg inte till text</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="664"/>
        <location filename="../tabwidget.ui" line="806"/>
        <location filename="../tabwidget.ui" line="1050"/>
        <location filename="../tabwidget.ui" line="1276"/>
        <location filename="../tabwidget.ui" line="1481"/>
        <location filename="../tabwidget.ui" line="1629"/>
        <location filename="../tabwidget.ui" line="1658"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="681"/>
        <source>Extensions</source>
        <translation>Filnamnstillägg</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="727"/>
        <source>Custom file extensions, patterns</source>
        <translation>Anpassade filnamnstillägg, mönster</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="740"/>
        <location filename="../tabwidget.ui" line="866"/>
        <source>New</source>
        <translation>Ny</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="766"/>
        <location filename="../tabwidget.ui" line="892"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="779"/>
        <location filename="../tab_extensions.cpp" line="61"/>
        <source>Name [*.extension1 *.extension2] (*.extension1 *.extension2)</source>
        <translation>Namn [*.filnamnstillägg1 *.filnamnstillägg2] (*.filnamnstillägg1 *.filnamnstillägg2)</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="853"/>
        <source>Be careful if you add your own file extensions!</source>
        <translation>Var försiktig om du lägger till din egna filändelse!</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="905"/>
        <source>Allowed file extensions</source>
        <translation>Tillåtna filnamnstillägg</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="939"/>
        <source>Enter a default location to copy to</source>
        <translation>Ange en förvald plats att kopiera till</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1197"/>
        <source>Check for updates when the program starts</source>
        <translation>Kontrollera efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1229"/>
        <source>Use the operating system&apos;s own dialogs</source>
        <translation>Använd operativsystemets egna dialogrutor</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1317"/>
        <source>Desktop shortcut</source>
        <translation>Genväg på skrivbordet</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1330"/>
        <source>Show tooltips</source>
        <translation>Visa verktygstips</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1362"/>
        <source>Force Update</source>
        <translation>Tvinga uppdatering</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1391"/>
        <source>Display Failure</source>
        <translation>Visa misslyckade namnändringar</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1407"/>
        <source>Display Success</source>
        <translation>Visa framgångsrika namnändringar</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1468"/>
        <source>Choose what you want to display in the toolbar</source>
        <translation>Välj vad du vill visa i verktygsfältet</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1601"/>
        <source>Pre-check language file</source>
        <translation>Förhandskolla språkfil</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="792"/>
        <source>Filter by file extension at &quot;Rename&quot; and &quot;Copy&quot;</source>
        <translation>Filtrera efter filnamnstillägg vid &quot;Namnändra&quot; och &quot;Kopiera&quot;</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="514"/>
        <source>Folder named after the year
the picture was taken</source>
        <translation>Mapp namngiven efter året
bilden togs</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="912"/>
        <source>Open / Copy to</source>
        <translation>Öppna / Kopiera till</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="964"/>
        <source>Select</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1021"/>
        <source>Always copy to default location without asking</source>
        <translation>Kopiera alltid till förvald plats utan att fråga</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1005"/>
        <source>Do not copy to default location</source>
        <translation>Kopiera aldrig till förvald plats</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1087"/>
        <source>Always open the last selected folder</source>
        <translation>Öppna alltid senast vald folder</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1103"/>
        <source>RadioButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1144"/>
        <source>Drag and drop renames the files</source>
        <translation>Dra och släpp namnändrar filerna</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1160"/>
        <source>Drag and drop copies the files</source>
        <translation>Dra och släpp kopierar filerna</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1170"/>
        <source>Misc.</source>
        <translation>Diverse</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1304"/>
        <source>Shortcut in the operating system menu</source>
        <translation>Genväg i operativsystemets meny</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1423"/>
        <source>Failures are written in red text</source>
        <translation>Misslyckanden skrivs med röd text</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1455"/>
        <source>Rename</source>
        <translation>Ändra namn</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1494"/>
        <source>Rename recursively</source>
        <translation>Namnändra rekursivt</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1507"/>
        <source>Copy and Rename</source>
        <translation>Kopiera och ändra namn</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1520"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1533"/>
        <source>Edit EXIF Date/Time</source>
        <translation>Redigera EXIF Datum/Tid</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1546"/>
        <source>Edit EXIF Date/Time and Rename</source>
        <translation>Redigera EXIF Datum/Tid och ändra namn</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1559"/>
        <source>Edit EXIF Date/Time with offset</source>
        <translation>Redigera EXIF Datum/Tid med offset</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1572"/>
        <source>Edit EXIF Date/Time with offset and Rename</source>
        <translation>Redigera EXIF Datum/Tid med offset och namnändra</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1608"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="1637"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="592"/>
        <source>In the end</source>
        <translation>På slutet</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="451"/>
        <source>Keep the original
lowercase or UPPERCASE</source>
        <translation>Behåll orginalet
gemener eller VERSALER</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="497"/>
        <source>Folder named after
the file name extension</source>
        <translation>Mapp namngiven efter
filnamnstillägget</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="605"/>
        <source>Save or remove text</source>
        <translation>Spara eller ta bort text</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="621"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="634"/>
        <location filename="../tabwidget.ui" line="753"/>
        <location filename="../tabwidget.ui" line="879"/>
        <source>Remove</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../tab_setgetconfig.cpp" line="146"/>
        <location filename="../tab_setgetconfig.cpp" line="149"/>
        <location filename="../tab_widget.cpp" line="133"/>
        <location filename="../tab_widget.cpp" line="141"/>
        <source>.extension</source>
        <translation>.filnamnstillägg</translation>
    </message>
    <message>
        <location filename="../tab_setgetconfig.cpp" line="163"/>
        <source>extension</source>
        <translation>filnamnstillägg</translation>
    </message>
    <message>
        <location filename="../tabwidget.ui" line="484"/>
        <source>Only time in file name</source>
        <translation>Endast tid i filnamnet</translation>
    </message>
    <message>
        <location filename="../tab_setgetconfig.cpp" line="143"/>
        <location filename="../tab_widget.cpp" line="125"/>
        <source>.EXTENSION</source>
        <translation>.FILNAMNSTILLÄGG</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="28"/>
        <location filename="../tab_extensions.cpp" line="111"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="29"/>
        <location filename="../tab_extensions.cpp" line="112"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="32"/>
        <source>Do you want to delete all saved file extensions and restore the file extension filters to the default values?</source>
        <translation>Vill du ta bort alla sparade filtillägg och återställa filtilläggfiltren till standardvärdena?</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="58"/>
        <location filename="../tab_extensions.cpp" line="140"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="59"/>
        <location filename="../tab_extensions.cpp" line="141"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="62"/>
        <source>My image files [*.jpg *.rw2] (*.jpg *.rw2)</source>
        <translation>Mina bildfiler [*.jpg *.rw2] (*.jpg *.rw2)</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="115"/>
        <source>Do you want to delete all saved file extensions and restore the file extension to the default values?</source>
        <translation>Vill du ta bort alla sparade filändelser och återställa till standardfiländelserna?</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="143"/>
        <source>The file name extension, without the preceding dot.</source>
        <translation>Filnamnstillägget, utan föregående punkt.</translation>
    </message>
    <message>
        <location filename="../tab_extensions.cpp" line="150"/>
        <source>Incorrect file name extension.
Permitted characters are a-z, A-Z and 0-9.</source>
        <translation>Felaktigt filnamnstillägg.
Tillåtna tecken är a-z, A-Z och 0-9.</translation>
    </message>
    <message>
        <location filename="../tab_widget.cpp" line="275"/>
        <source>Always open</source>
        <translation>Öppna alltid</translation>
    </message>
    <message>
        <location filename="../tab_widget.cpp" line="356"/>
        <source>An unexpected error occurred during the forced update.</source>
        <translation>Ett oväntat fel inträffade under den tvingade uppdateringen.</translation>
    </message>
    <message>
        <location filename="../tab_copyto.cpp" line="68"/>
        <source>The operating system does not allow you to save files in</source>
        <translation>Operativsystemet tillåter inte att du sparar filer i</translation>
    </message>
    <message>
        <location filename="../tab_copyto.cpp" line="68"/>
        <source>check your file permissions.</source>
        <translation>kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../tab_shortcuts.cpp" line="61"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas i
&quot;~ / .local / share / applikationer&quot;
Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../tab_shortcuts.cpp" line="103"/>
        <location filename="../tab_shortcuts.cpp" line="105"/>
        <source>Rename image files so that the name contains the day and time the image was taken</source>
        <translation>Byt namn på bildfiler så att namnet innehåller dagen och tiden då bilden togs</translation>
    </message>
    <message>
        <location filename="../tab_shortcuts.cpp" line="126"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas.
Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="41"/>
        <source>Open your language file</source>
        <translation>Öppna din språkfil</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="41"/>
        <source>Compiled language file (*.qm)</source>
        <translation>Kompilerad språkfil (*.qm)</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="49"/>
        <source>Shut down and restart</source>
        <translation>Stäng och starta igen</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="49"/>
        <source>and your selected language file will be loaded.</source>
        <translation>och ditt valda språk kommer att laddas in.</translation>
    </message>
</context>
</TS>

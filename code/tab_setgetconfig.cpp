//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "tabwidget.h"
#include "mainwindow.h"
#include "ui_tabwidget.h"
void TabWidget::setStartConfig()
{
#ifdef Q_OS_LINUX
    ui->checkNativeDialog->setChecked(false);
    ui->checkNativeDialog->setDisabled(true);
#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Tooltips");

    if(settings.value("showtooltips", true).toBool()) {
        ui->checkToolTips->setChecked(true);
    } else {
        ui->checkToolTips->setChecked(false);
    }

    settings.endGroup();
    settings.beginGroup("PathPattern");
    QString datetimepattern = settings.value("datetimepattern", "_").toString();
    QString folderpattern = settings.value("folderpattern", "-").toString();
    QString filenamepattern = settings.value("filenamepattern", "-").toString();
    bool onlytimeinfilename = settings.value("onlytimeinfilename", false).toBool();
    /* Folder for Years */
    bool yearsinfoldernamepattern =
        settings.value("yearsinfoldernamepattern", true).toBool();
    bool extensioninfoldernamepattern =
        settings.value("extensioninfoldernamepattern", true).toBool();
    QStringList insert = settings.value("insert").toStringList();
    settings.endGroup();
    settings.beginGroup("Tools");
    QString radiotext = settings.value("radiotext", "radionotext").toString();
#ifndef Q_OS_LINUX
    ui->checkNativeDialog->setChecked(settings.value("usenativedialog", true).toBool());
#endif
    ui->checkDoCheckOnStart->setChecked(settings.value("checkonstart", true).toBool());
    ui->checkDisplayFailure->setChecked(settings.value("displayfailure", true).toBool());
    ui->checkDisplaySuccess->setChecked(settings.value("displaysuccess", false).toBool());
    bool alwayscopytodefaultlocation = settings.value("alwayscopytodefaultlocation", false).toBool();
    bool checkredtext = settings.value("checkredtext", true).toBool();
    ui->radioLastUsed->setChecked(settings.value("useopenpath", false).toBool());
    ui->radioHome->setChecked(!settings.value("useopenpath", true).toBool());
    QString fileextension  = settings.value("fileextension", "keep").toString();

    if(settings.value("draganddrop", "rename").toString() == "rename") {
        ui->radioDragDroppRename->setChecked(true);
    } else {
        ui->radioDragDroppCopy->setChecked(true);
    }

    settings.endGroup();
    settings.beginGroup("PathPattern");
    QString currentinserttext = settings.value("currentinserttext").toString();
    settings.endGroup();
    settings.beginGroup("Extensions");
    QStringList selectedfilters = settings.value("selectedfilters").toStringList();
    selectedfilters.removeDuplicates();
    selectedfilters.sort(Qt::CaseInsensitive);
    QStringList selectedextensions = settings.value("selectedextensions").toStringList();
    selectedextensions.removeDuplicates();
    selectedextensions.sort(Qt::CaseInsensitive);
    settings.endGroup();
    settings.beginGroup("Path");
    QString defaultcopypath = settings.value("defaultcopypath", QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)).toString();
    QString copypath = settings.value("copypath").toString();
    settings.setValue("copypath", copypath);

    if(pathExist(defaultcopypath)) {
        ui->lblCopyLocation->setText(defaultcopypath);
        ui->radioCopyToDefault->setEnabled(true);

        if(alwayscopytodefaultlocation) {
            ui->radioCopyToDefault->setChecked(true);
        } else {
            ui->radioCopyToDefault->setChecked(false);
        }
    } else {
        ui->lblCopyLocation->setText("");
        ui->radioCopyToDefault->setDisabled(true);

        if(alwayscopytodefaultlocation) {
            ui->radioCopyToDefault->setChecked(true);
        } else {
            ui->radioCopyToDefault->setChecked(false);
        }
    }

    settings.endGroup();
    settings.beginGroup("Toolbar");
    ui->checkActionClose->setChecked(settings.value("close", true).toBool());
    ui->checkActionClear->setChecked(settings.value("clear", true).toBool());
    ui->checkActionCopyRename->setChecked(settings.value("copyrename", true).toBool());
    ui->checkActionEditExif->setChecked(settings.value("editexif", false).toBool());
    ui->checkActionEditExifOffset->setChecked(settings.value("editexifoffset", false).toBool());
    ui->checkActionEditExifOffsetRename->setChecked(settings.value("editexifoffsetrename", false).toBool());
    ui->checkActionEditExifRename->setChecked(settings.value("editexifrename", false).toBool());
    ui->checkActionRename->setChecked(settings.value("rename", true).toBool());
    ui->checkActionRenameRecursively->setChecked(settings.value("renamerecursively", false).toBool());
    settings.endGroup();

    if(checkredtext) {
        ui->checkRedText->setChecked(true);
    } else {
        ui->checkRedText->setChecked(false);
    }

    if(radiotext == "radionotext") {
        ui->radioNoText->setChecked(true);
    } else if(radiotext == "radiobefore") {
        ui->radioBefore->setChecked(true);
    } else {
        ui->radioAfter->setChecked(true);
    }

    ui->cmbInsert->clear();
    ui->cmbInsert->addItems(insert);
    //    ui->cmbInsert->setCurrentIndex(textindex);
    ui->cmbInsert->setCurrentText(currentinserttext);
    QString ext;

    if(fileextension == "capital") {
        ui->radioCapital->setChecked(true);
        ext = tr(".EXTENSION");
    } else if(fileextension == "lowercase") {
        ui->radioLowercase->setChecked(true);
        ext = tr(".extension");
    } else {
        ui->radioKeep->setChecked(true);
        ext = tr(".extension");
    }

    QDateTime now = QDateTime::currentDateTime();
    // yyyy-MM-dd_hh-mm-ss
    QString folder_now(now.toTimeSpec(Qt::LocalTime).toString("yyyy" + folderpattern + "MM" + folderpattern + "dd"));

    /* Folder for Years */
    if(yearsinfoldernamepattern) {
        folder_now =
            folder_now.left(4) + QDir::toNativeSeparators("/") + folder_now;
    }

    if(extensioninfoldernamepattern) {
        folder_now = folder_now + QDir::toNativeSeparators("/") + tr("extension");
    }

    QString filename_now(now.toTimeSpec(Qt::LocalTime)
                         .toString("yyyy" + filenamepattern + "MM" +
                                   filenamepattern + "dd" + datetimepattern +
                                   "hh" + filenamepattern + "mm" +
                                   filenamepattern + "ss") + ext);
    QString filename_now_short(
        now.toTimeSpec(Qt::LocalTime)
        .toString("hh" + filenamepattern + "mm" + filenamepattern + "ss") + ext);
    ui->lblFolder->setText(folder_now);

    if(onlytimeinfilename) {
        ui->lblCurrentDateTime->setText(filename_now_short);
        ui->cmbDateTime->setDisabled(true);
    } else {
        ui->lblCurrentDateTime->setText(filename_now);
        ui->cmbDateTime->setEnabled(true);
    }

    /* Folder for Years */
    ui->checkYears->setChecked(yearsinfoldernamepattern);
    ui->checkExtension->setChecked(extensioninfoldernamepattern);

    if(filenamepattern == " ") {
        ui->cmbFileName->setCurrentIndex(0);
    } else if(filenamepattern == "-") {
        ui->cmbFileName->setCurrentIndex(1);
    } else {
        ui->cmbFileName->setCurrentIndex(2);
    }

    if(datetimepattern == " ") {
        ui->cmbDateTime->setCurrentIndex(0);
    } else if(datetimepattern == "-") {
        ui->cmbDateTime->setCurrentIndex(1);
    } else {
        ui->cmbDateTime->setCurrentIndex(2);
    }

    if(folderpattern == " ") {
        ui->cmbFolderName->setCurrentIndex(0);
    } else if(folderpattern == "-") {
        ui->cmbFolderName->setCurrentIndex(1);
    } else {
        ui->cmbFolderName->setCurrentIndex(2);
    }

    ui->checkOnlyTime->setChecked(onlytimeinfilename);
    ui->cmbExtensions->addItems(selectedfilters);
    ui->cmbExtensions_2->addItems(selectedextensions);
    const QString shortcutlocationdesktop = QDir::toNativeSeparators(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));
    const QString shortcutlocationapplications = QDir::toNativeSeparators(QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation));
// Desktop
#ifdef Q_OS_WIN
    QString link(QDir::toNativeSeparators(shortcutlocationdesktop + "/EXIF ReName.lnk"));
#endif
#ifdef Q_OS_LINUX
    QString link(QDir::toNativeSeparators(shortcutlocationdesktop + "/exifrename.desktop"));
#endif
    bool shortcutexists = chortcutExists(link);
    bool shortisececutable = chortcutIsExecutable(link);
    ui->checkChortcutDesktopLocation->setChecked(shortcutexists && shortisececutable);
// ApplicationsLocation
#ifdef Q_OS_WIN
    link = QDir::toNativeSeparators(shortcutlocationapplications + "/EXIF ReName.lnk");
#endif
#ifdef Q_OS_LINUX
    link = QDir::toNativeSeparators(shortcutlocationapplications + "/exifrename.desktop");
#endif
    shortcutexists = chortcutExists(link);
    shortisececutable = chortcutIsExecutable(link);
    ui->checkChortcutApplicationsLocation->setChecked(shortcutexists && shortisececutable);
    settings.beginGroup("Path");
    settings.setValue("copypath", copypath);
    settings.endGroup();
    QPalette p = ui->textBrowserLicense->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, QColor(RED, GREEN, BLUE)); // set color "Red" for textedit base
    //    p.setColor(QPalette::Text, QColor(0, 0, 0)); // set text color which is selected from color pallete
    ui->textBrowserLicense->setPalette(p); // change textedit palette
}

void TabWidget::setEndConfig()
{
    if(ui->cmbInsert->currentText() != "") {
        QStringList insert;

        for(int i = 0; i < ui->cmbInsert->count(); i++) {
            insert << ui->cmbInsert->itemText(i);
        }

        if(!insert.contains(ui->cmbInsert->currentText())) {
            ui->cmbInsert->addItem(ui->cmbInsert->currentText());
            //            ui->cmbInsert->setCurrentIndex(ui->cmbInsert->findData(ui->cmbInsert->currentText()));
        }
    }

    QString currentinserttext = ui->cmbInsert->currentText();
    QStringList insert;

    for(int i = 0; i < ui->cmbInsert->count(); i++) {
        insert << ui->cmbInsert->itemText(i);
    }

    QString fileextension;

    if(ui->radioCapital->isChecked()) {
        fileextension = "capital";
    } else if(ui->radioLowercase->isChecked()) {
        fileextension = "lowercase";
    } else {
        fileextension = "keep";
    }

    insert.removeAll({});
    insert.removeDuplicates();
    insert.sort();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Tooltips");

    if(ui->checkToolTips->isChecked()) {
        settings.setValue("showtooltips", true);
    } else {
        settings.setValue("showtooltips", false);
    }

    settings.beginGroup("PathPattern");
    settings.setValue("insert", insert);
    settings.setValue("currentinserttext", currentinserttext);
    settings.setValue("datetimepattern", indexToChar(ui->cmbDateTime->currentIndex()));
    settings.setValue("filenamepattern", indexToChar(ui->cmbFileName->currentIndex()));
    settings.setValue("folderpattern", indexToChar(ui->cmbFolderName->currentIndex()));
    /* */
    /* */
    settings.setValue("onlytimeinfilename", ui->checkOnlyTime->isChecked());
    /* Folder for Years */
    settings.setValue("yearsinfoldernamepattern", ui->checkYears->isChecked());
    settings.setValue("extensioninfoldernamepattern", ui->checkExtension->isChecked());
    settings.endGroup();
    settings.beginGroup("Tools");
    settings.setValue("fileextension", fileextension);
#ifndef Q_OS_LINUX
    settings.setValue("usenativedialog", ui->checkNativeDialog->isChecked());
#endif
    settings.setValue("alwayscopytodefaultlocation", ui->radioCopyToDefault->isChecked());
    settings.setValue("checkonstart", ui->checkDoCheckOnStart->isChecked());
    settings.setValue("displaysuccess", ui->checkDisplaySuccess->isChecked());
    settings.setValue("displayfailure", ui->checkDisplayFailure->isChecked());
    settings.setValue("useopenpath", ui->radioLastUsed->isChecked());
    QString radiotext;

    if(ui->radioNoText->isChecked()) {
        radiotext = "radionotext";
    } else if(ui->radioBefore->isChecked()) {
        radiotext = "radiobefore";
    } else {
        radiotext = "radioafter";
    }

//    settings.beginGroup("Tools");

    if(ui->radioDragDroppRename->isChecked()) {
        settings.setValue("draganddrop", "rename");
    } else {
        settings.setValue("draganddrop", "copy");
    }

    settings.setValue("radiotext", radiotext);
    // ipac
    settings.setValue("checkredtext", ui->checkRedText->isChecked());
    settings.endGroup();
    settings.beginGroup("Path");
    settings.setValue("defaultcopypath", ui->lblCopyLocation->text());
    settings.endGroup();
    settings.beginGroup("Toolbar");
    settings.setValue("close", ui->checkActionClose->isChecked());
    settings.setValue("clear", ui->checkActionClear->isChecked());
    settings.setValue("copyrename", ui->checkActionCopyRename->isChecked());
    settings.setValue("editexif", ui->checkActionEditExif->isChecked());
    settings.setValue("editexifoffset", ui->checkActionEditExifOffset->isChecked());
    settings.setValue("editexifoffsetrename", ui->checkActionEditExifOffsetRename->isChecked());
    settings.setValue("editexifrename",  ui->checkActionEditExifRename->isChecked());
    settings.setValue("rename",  ui->checkActionRename->isChecked());
    settings.setValue("renamerecursively", ui->checkActionRenameRecursively->isChecked());
    settings.endGroup();
}

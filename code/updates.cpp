//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "info.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
void MainWindow::doCheckForUpdates()
{
    const QString updateinstructions = UpdateInstructions();
    CheckUpdate *cu = new CheckUpdate;
    cu->check(DISPLAY_NAME, VERSION, VERSION_PATH, updateinstructions);
    connect(cu, &CheckUpdate::foundUpdate, [this](bool b) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif

        if(b) {
            updateNeeded(true);
            ui->actionUpdate->setEnabled(true);
        } else {
            updateNeeded(false);
        }
    });
}

void MainWindow::checkOnStart()
{
    const QString updateinstructions = UpdateInstructions();
    CheckUpdate *cu = new CheckUpdate;
    cu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, updateinstructions);
    connect(cu, &CheckUpdate::foundUpdate, [this](bool b) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif

        if(b) {
            updateNeeded(true);
#ifdef Q_OS_LINUX
            ui->actionUpdate->setEnabled(true);
#endif
        } else {
            updateNeeded(false);
#ifdef Q_OS_LINUX
            ui->actionUpdate->setDisabled(true);
#endif
        }
    });
}

QString MainWindow::UpdateInstructions()
{
#ifdef Q_OS_LINUX
    QString *updateinstructions =
        new QString(QObject::tr("Select \"Tools\", \"Update\" to update."));
#endif
#ifdef Q_OS_WIN
#ifdef portable
    QString *updateinstructions =
        new QString(QObject::tr("Download a new") + " <a href=\"" DOWNLOAD_PATH
                    "\"> portable</a>");
#endif
#ifndef portable
    QString *updateinstructions = new QString(QObject::tr(
            "Select \"Tools\", \"Update / Uninstall\" and \"Update component\"."));
#endif
#endif
    return *updateinstructions;
}

void MainWindow::updateNeeded(bool needed)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Updates");
    settings.setValue("updateneeded", needed);
    settings.endGroup();
}

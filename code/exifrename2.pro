#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          EXIF ReName
#//          Copyright (C) 2007 - 2021 Ingemar Ceicer
#//          https://gitlab.com/posktomten/exifrename2
#//          ic_0002 (at) ceicer (dot) com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT       += core gui network widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#CONFIG += c++11
#QMAKE_CXXFLAGS += -std=c++17

#DEFINES += _CRT_SECURE_NO_WARNINGS
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES += QT_DEPRECATED_WARNINGS
TARGET = exifrename

SOURCES += \
    change_exif.cpp \
    change_exif_manyfiles.cpp \
    change_exif_offset.cpp \
    change_exif_offset_manyfiles_rename.cpp \
    findexif.cpp \
    firstrun.cpp \
    font.cpp \
    inputdialog.cpp \
    inputdialogoffset.cpp \
    langguage.cpp \
    main.cpp \
    mainwindow.cpp \
    exif.cpp \
    open.cpp \
    open_copy.cpp \
    open_recursively.cpp \
    rename.cpp \
    setgetconfig.cpp \
    tab_copyto.cpp \
    tab_extensions.cpp \
    tab_firstrun.cpp \
    tab_pathexist.cpp \
    tab_placeonscreen.cpp \
    tab_setgetconfig.cpp \
    tab_shortcuts.cpp \
    info.cpp \
    tab_widget.cpp \
    updates.cpp \
    zoom.cpp \
    exifw.cpp \
    selectfont.cpp \
    test_translation.cpp

HEADERS += \
    # filters.h \
    inputdialog.h \
    inputdialogoffset.h \
#    inputdialog_global.h \
    mainwindow.h \
    exif.h \
    tabwidget.h \
    info.h \
    exifw.h \
    selectfont.h \
    checkupdate_global.h

FORMS += \
    dialogoffset.ui \
    mainwindow.ui \
    tabwidget.ui \
    selectfont.ui


equals(QT_MAJOR_VERSION, 5) {
    DESTDIR="../build-executable5"
    LIBS += "-L../lib5"
    LIBS += -lcheckupdate
    unix:LIBS += -lupdateappimage
    unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lcrypto # Release
    unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lssl # Release

}


equals(QT_MAJOR_VERSION, 6) {
    DESTDIR="../build-executable6"
    LIBS += "-L../lib6"
    LIBS += -lcheckupdate
    LIBS += -labout
    LIBS += -lcreateshortcut
    LIBS += -ldownloadunpack
    LIBS += -lupdateappimage
    # unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lcrypto # Release
    # unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lssl # Release

}
INCLUDEPATH += "../include"

TRANSLATIONS += \
    i18n/_exifrename2_sv_SE.ts \
    i18n/_exifrename2_templete_xx_XX.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

CODECFORSRC = UTF-8

# DESTDIR="C:\Program Files\EXIF ReName"

# UI_DIR = ../code
win32:RC_ICONS += images/exifrename.ico

RESOURCES += \
    resource.qrc

//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "tabwidget.h"
#include "filters.h"
void MainWindow::firstRun()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("FirstRun");
    bool firstrun = settings.value("firstrun", true).toBool();
    settings.setValue("firstrun", firstrun);
    settings.endGroup();
    settings.sync();

    if(!firstrun) {
        return;
    }

#ifdef Q_OS_LINUX
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    QString iconpath = QDir::toNativeSeparators(fi.absolutePath());
    QDir pathDir(iconpath);

    if(pathDir.exists()) {
        iconpath.append(QDir::toNativeSeparators("/exifrename.png"));
        const QString pngpath = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/exifrename.png");

        if(QFile::exists(iconpath)) {
            QFile::remove(iconpath);
        }

        if(QFile::copy(pngpath, iconpath)) {
            QFile file(iconpath);
            file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
        }
    }

#endif
    settings.beginGroup("FirstRun");
    settings.setValue("firstrun", false);
    settings.endGroup();
    settings.beginGroup("Extensions");
    // settings.setValue("selectedfilters", Filters::allFilters());
    // settings.setValue("selectedextensions", Filters::allExtensions());
    settings.endGroup();
    /* */
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    int width = screenGeometry.width();
    int height = screenGeometry.height();
    QMessageBox msgBox(this);
    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox.setStyleSheet("font:22px;");
    msgBox.setText(tr("Welcome to EXIF ReName!"));
    msgBox.setInformativeText(tr("Before you begin, please take a look at the settings."));
    msgBox.setStandardButtons(QMessageBox::Ok);
    int w = msgBox.width();
    int h = msgBox.height();
    msgBox.move((width / 2) - w, (height / 2) - h);
    msgBox.exec();
    TabWidget *tabwidget = new TabWidget(nullptr);
    tabwidget->setCurrentIndex(5);
    tabwidget->show();
}

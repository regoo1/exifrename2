//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "exif.h"
#include "mainwindow.h"
#include "tabwidget.h"
#include "ui_mainwindow.h"

void MainWindow::connectLanguage()
{
    connect(ui->actionEnglish, &QAction::triggered, [ this ]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Language");
        QString sp = settings.value("language", "none").toString();
        settings.endGroup();

        if(sp != "en_US") {
            QMessageBox *msgBox = new QMessageBox(this);
            QAbstractButton *cancelButton = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
            QAbstractButton *restartButton = msgBox->addButton(tr("Restart Now"), QMessageBox::YesRole);
            msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("The program must be restarted for the new language settings to take effect."));
            msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
            msgBox->exec();

            if(msgBox->clickedButton() == restartButton) {
                settings.beginGroup("Language");
                settings.setValue("language", "en_US");
                settings.endGroup();
                const QString EXECUTE = QDir::toNativeSeparators(
                                            QCoreApplication::applicationDirPath() + "/" +
                                            QFileInfo(QCoreApplication::applicationFilePath()).fileName());
                QProcess p;
                p.setProgram(EXECUTE);
                p.startDetached();
                close();
            }

            delete cancelButton;
        }
    });
    connect(ui->actionSvenska, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        settings.setIniCodec("UTF-8");
#endif
        settings.beginGroup("Language");
        QString sp = settings.value("language", "none").toString();
        settings.endGroup();

        if(sp != "sv_SE") {
            QMessageBox *msgBox = new QMessageBox(this);
            QAbstractButton *cancelButton = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
            QAbstractButton *restartButton = msgBox->addButton(tr("Restart Now"), QMessageBox::YesRole);
            msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("The program must be restarted for the new language settings to take effect."));
            msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
            msgBox->exec();

            if(msgBox->clickedButton() == restartButton) {
                settings.beginGroup("Language");
                settings.setValue("language", "sv_SE");
                settings.endGroup();
                const QString EXECUTE = QDir::toNativeSeparators(
                                            QCoreApplication::applicationDirPath() + "/" +
                                            QFileInfo(QCoreApplication::applicationFilePath()).fileName());
                QProcess p;
                p.setProgram(EXECUTE);
                p.startDetached();
                close();
            }

            delete cancelButton;
        }
    });
}

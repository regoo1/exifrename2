//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "tabwidget.h"
#include "mainwindow.h"
#include "ui_tabwidget.h"
#include "ui_mainwindow.h"

void TabWidget::selectCopyTo()
{
    QDir dir(QDir::home());
    QString home = QDir::toNativeSeparators(dir.absolutePath());
    QFileDialog * dialog = new QFileDialog(this);
    dialog->resize(900, 600);
    dialog->setOption(QFileDialog::DontUseNativeDialog, !ui->checkNativeDialog->checkState());
    // dialog->setFileMode(QFileDialog::DirectoryOnly);
    dialog->setFileMode(QFileDialog::Directory);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Path");
    QString defaultcopypath = settings.value("defaultcopypath", home).toString();
    settings.endGroup();

    if(defaultcopypath.isEmpty()) {
        dialog->setDirectory(home);
    } else {
        dialog->setDirectory(defaultcopypath);
    }

    if(dialog->exec()) {
        QDir dir = dialog->directory();
        defaultcopypath = dir.path();

        /* */
        if(pathExist(defaultcopypath)) {
            /* */
            settings.beginGroup("Path");
            settings.setValue("defaultcopypath", defaultcopypath);
            settings.endGroup();
            ui->lblCopyLocation->setText(QDir::toNativeSeparators(defaultcopypath));
            settings.beginGroup("Tools");
            settings.endGroup();
            ui->radioCopyToDefault->setEnabled(true);
        } else {
            settings.beginGroup("Path");
            settings.setValue("defaultcopypath", "");
            settings.endGroup();
            ui->lblCopyLocation->setText("");
            settings.beginGroup("Tools");
            settings.setValue("alwayscopytodefaultlocation", false);
            settings.endGroup();
            ui->radioCopyToDefault->setDisabled(true);
            QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                                  tr("The operating system does not allow you to save files in") + "\n" + QDir::toNativeSeparators(defaultcopypath) + "\n" + tr("check your file permissions."));
        }
    }
}

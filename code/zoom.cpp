//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "ui_mainwindow.h"
void MainWindow::zoom()
{
    connect(ui->actionDefault, &QAction::triggered, this, &MainWindow::zoomDefault);
    connect(ui->actionOut, &QAction::triggered,  this, &MainWindow::zoomMinus);
    connect(ui->actionIn, &QAction::triggered, this, &MainWindow::zoomPlus);
    connect(ui->textBrowserMain, &QTextEdit::textChanged, this, &MainWindow::textBrowserMaintextChanged);
}

void MainWindow::textBrowserMaintextChanged()
{
    QTextCursor cursor = ui->textBrowserMain->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->textBrowserMain->setTextCursor(cursor);
}

void MainWindow::zoomDefault()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.setIniCodec("UTF-8");
#endif
    settings.beginGroup("Font");
    int pointsize = settings.value("size", FONTSIZE).toInt();
    settings.endGroup();
    QFont f = ui->textBrowserMain->font();
    f.setPointSize(pointsize);
    ui->textBrowserMain->setFont(f);
    QTextCursor cursor = ui->textBrowserMain->textCursor();
    ui->textBrowserMain->selectAll();
    ui->textBrowserMain->setFontPointSize(pointsize);
    ui->textBrowserMain->setTextCursor(cursor);
    ui->textBrowserMain->zoomOut();
}

void MainWindow::zoomMinus()
{
    QFont f = ui->textBrowserMain->font();
    int pointsize = f.pointSize();

    if(pointsize > 1) {
        QTextCursor cursor = ui->textBrowserMain->textCursor();
        ui->textBrowserMain->selectAll();
        ui->textBrowserMain->setFontPointSize(pointsize - 1);
        ui->textBrowserMain->setTextCursor(cursor);
        ui->textBrowserMain->zoomOut(1);
    }
}

void MainWindow::zoomPlus()
{
    QFont f = ui->textBrowserMain->font();
    int pointsize = f.pointSize();
    QTextCursor cursor = ui->textBrowserMain->textCursor();
    ui->textBrowserMain->selectAll();
    ui->textBrowserMain->setFontPointSize(pointsize  + 1);
    ui->textBrowserMain->setTextCursor(cursor);
    ui->textBrowserMain->zoomIn(1);
}

//void MainWindow::wheelEvent(QWheelEvent *ev)
//{
//    if(QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
//        QPoint numDegrees = ev->angleDelta();
//        int direction = numDegrees.y();

//        if(direction < 0) {
//            zoomMinus();
//        } else if(direction > 0) {
//            zoomPlus();
//        }

//        ev->accept();
//    }
//}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="vanished">Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <source>Your version of </source>
        <translation type="vanished">Din version av </translation>
    </message>
    <message>
        <source> is newer than the latest official version. </source>
        <translation type="vanished"> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <source> is the same version as the latest official version. </source>
        <translation type="vanished"> är samma version som den senaste officiella versionen. </translation>
    </message>
    <message>
        <source>There was an error when the version was checked.</source>
        <translation type="vanished">Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <source>
There was an error when the version was checked.</source>
        <translation type="vanished">
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <source>Updates:</source>
        <translation type="vanished">Uppdateringar:</translation>
    </message>
    <message>
        <source>There is a new version of </source>
        <translation type="vanished">Det finns en ny version av </translation>
    </message>
    <message>
        <source>Latest version: </source>
        <translation type="vanished">Senaste versionen: </translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="48"/>
        <location filename="../checkupdate.cpp" line="120"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="65"/>
        <location filename="../checkupdate.cpp" line="73"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="66"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="74"/>
        <source> is the same version as the latest official version. </source>
        <translation> är samma version som den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="83"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="135"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="178"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="209"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="211"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
</context>
</TS>

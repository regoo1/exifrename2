# EXIF ReName

# A program to name the pictures after the date and time the picture was taken.

Uses Qt5 and Qt6, for Linux and Windows.

<h2>[Read more](https://gitlab.com/posktomten/exifrename2/-/wikis/Home)</h2>

<table><tr><td>

**My libraries that I reuse** <br>

<a href="https://gitlab.com/posktomten/libabout">libabout</a>

<a href="https://gitlab.com/posktomten/libzsyncupdateappimage">libzsyncupdateappimage</a>

<a href="https://gitlab.com/posktomten/libcreateshortcut_windows">libcreatechortcut (Windows)</a>

<a href="https://gitlab.com/posktomten/libcreateshortcut">libcreatechortcut (Linux)</a>

<a href="https://gitlab.com/posktomten/libcheckforupdates">libcheckforupdates</a>

<a href="https://gitlab.com/posktomten/download_offline_installer">download_offline_installer</a>



</td></tr></table>

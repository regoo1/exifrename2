linuxdeployqt
appimagetool

-unsupported-allow-new-glibc
-unsupported-bundle-everything


linuxdeployqt AppDir/usr/bin/exifrename -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -unsupported-allow-new-glibc
STATIC
export "VERSION=0.0.1"
linuxdeployqt AppDir/usr/bin/exifrename -appimage -bundle-non-qt-libs -verbose=2 -no-translations
linuxdeployqt AppDir/usr/bin/exifrename -appimage -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files

DYNAMIC Lubuntu 32
linuxdeployqt AppDir/usr/bin/exifrename -qmake=/opt/Qt5.15.2_shared/bin/qmake -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files

DYNAMIC Lubuntu 64
linuxdeployqt AppDir/usr/bin/exifrename -qmake=/opt/Qt/5.15.2/gcc_64/bin/qmake -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files
linuxdeployqt AppDir/usr/bin/exifrename -qmake=/opt/Qt/5.15.2/gcc_64/bin/qmake -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -unsupported-allow-new-glibc

appimagetool --sign AppDir/
appimagetool -n --sign AppDir/


Lubuntu 64, dynamic
-qmake=/opt/Qt/5.15.2/gcc_64/bin/qmake

Lubuntu 32, dynamic
-qmake=/opt/Qt5.15.2_shared/bin/qmake


Lubuntu 32-bit STATIC
/opt/Qt5.15.2_static/bin/lrelease ../code/exifrename2.pro
/opt/Qt5.15.2_static/bin/qmake -project ../code/exifrename2.pro
/opt/Qt5.15.2_static/bin/qmake ../code/exifrename2.pro

Lubuntu 32-bit DYNAMIC
/opt/Qt5.15.2_shared/bin/lrelease ../code/exifrename2.pro
/opt/Qt5.15.2_shared/bin/qmake -project ../code/exifrename2.pro
/opt/Qt5.15.2_shared/bin/qmake ../code/exifrename2.pro

Ubuntu 20.04 glibc 2.31
Ubuntu 18.04 glibc 2.27
Ubuntu 17.10 glibc 2.26
Ubuntu 16.04 glibc 2.23
Ubuntu 14.04 glibc 2.19


Ändra config.xml
	 Windows
	<RunProgram>@TargetDir@/exifrename.exe</RunProgram>

	Linux
	<RunProgram>@TargetDir@/AppRun</RunProgram>

	Windows 32-bit
	<TargetDir>@ApplicationsDirX84@/exifrename</TargetDir>
	<AdminTargetDir>@ApplicationsDirX86@/exifrename</AdminTargetDir>
	Windows 64-bit
	<TargetDir>@ApplicationsDirX86@/exifrename</TargetDir
	<AdminTargetDir>@ApplicationsDirX86@/exifrename</AdminTargetDir>


Ändra adress till repository i config.xml

	Linux 32-bit
	<Url>https://bin.ceicer.com/exifrename2/repository/linux_x86</Url>
	
	Linux 64-bit
	<Url>https://bin.ceicer.com/exifrename2/repository/linux_x86_64</Url>

	Windows 32-bit
	<Url>https://bin.ceicer.com/exifrename2/repository/win_x86</Url>
	
	Windows 64-bit
	<Url>https://bin.ceicer.com/exifrename2/repository/win_x86_64</Url>
 
 Generate online repository with

	repogen -p packages repository
	
	Linux 32-bit
	/opt/qtinstallerframework/bin/repogen -p packages linux_x86
	
	Linux 64-bit
	/opt/Qt/Tools/QtInstallerFramework/3.2/bin/repogen -p packages linux_x86_64

	Windows 32-bit
	C:\Qt\QtIFW-4.0.1\bin\repogen -p packages win_x86

	Window 64-bit
	C:\Qt\QtIFW-4.0.1\bin\repogen -p packages win_x86_64
	
	Skoldatorn
	C:\Qt\QtIFW-4.0.1\bin\repogen -p packages win_x86_64
  

Generate online installer

	Linux 32-bit
	/opt/qtinstallerframework/bin/binarycreator --online-only --resources fonts.qrc -c config/config.xml -p packages install-exifrename_x86-0.19.0
	
	Linux 64-bit
	/opt/Qt/Tools/QtInstallerFramework/3.2/bin/binarycreator --online-only -c config/config.xml -p packages install-streamCapture-0.20.2_x86_64.exe

	
	Windows 32-bit
     C:\Qt\QtIFW-4.0.1\bin\binarycreator --online-only -c config\config.xml -p packages install-EXIF_ReName_x86.exe
	 
	Windows 64-bit
	C:\Qt\QtIFW-4.0.1\bin\binarycreator --online-only -c config\config.xml -p packages install-EXIF_ReName_x86_64.exe
	
	Skoldatorn
	C:\Qt\QtIFW-4.0.1\bin\binarycreator --online-only -c config\config.xml -p packages install-exifrename_x86_64.exe

To deploy an update
	Ändra 
	<Version>2.5.2</Version> i package.xml

  repogen --update-new-components -p packages repository
  
  Linux 32-bit
  /opt/qtinstallerframework/bin/repogen --update-new-components -p packages linux_x86
  
   Linux 64-bit
  /opt/Qt/Tools/QtInstallerFramework/3.2/bin//repogen --update-new-components -p packages linux_x86
  
  Windows 32-bit
  C:\Qt\Qt\Tools\QtInstallerFramework\3.2\bin\repogen --update-new-components -p packages win_x86
  
  Windows  64-bit
  C:\Qt\Qt\Tools\QtInstallerFramework\3.2\bin\repogen --update-new-components -p packages win_x86_64










